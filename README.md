ToFuel NSW
==============

ToFuel NSW is an Android app for the Udacity Nanodegree Project 7/8.

### DESCRIPTION:
As the economy grows, so does cost of living, especially if you live in NSW metro area. Nowadays, we use vehicles, not only forleisure activities, but for work as well. How many times have you filled up your car with fuel and to find another station down the road that’s 20c/litre cheaper? Every cent counts at this day of age and this is where ToFuel NSW can assist you with that.

ToFuel NSW aims to provide road commuters locations of petrol stations and their petrol prices within New South Wales, Australia. Using users’ location, ToFuel NSW can search for nearby petrol stations, and the results can be sorted by closest to the user or cheapest by petrol price. If users’ doesn’t give permission to his/her location, then they can alternatively search a location.

ToFuel NSW allows users to get direction to the station, via Google Maps. Additionally, users can share the fuel details of the selected station to others, via their favourite apps. Moreover, users (if logged in) can save their favourite stations so repetitive search queries can be avoided and also they’re able to review the station.

<div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>  


### INTENDED USER:
The intended users are road commuters within New South Wales, Australia. The API is relatively new and currently supports the major cities within New South Wales, such as Sydney, Parramatta, Bankstown, etc.

Note to reviewer:
The app's objectives is to find fuel prices within NSW, Australia via searching a location or device's location. The latter requires the device to be within NSW region, therefore an emulator is needed to test it if you're located outside of NSW, Australia. Thank you for understanding.


### UPDATES:
19-12-2018: Minor bug fixes and added an Up button in Results screen.  
06-12-2018: Minor bug fixes involving UI, added a couple more warning/error messages when required.  
01-11-2018: Major Bug Fixes, includes loading bar keeps looping when fetching location, incorrect deletion of searched history and unexpected behaviour when rotating screen.  
02-07-2018: Re-designed Widgets, now it takes individiual stations rather than a list-full.  
27-02-2018: Added Widget click support and refresh.  
26-02-2018: Re-designed Favourites screen and widget. Optimised operations for better user experience.  
09-02-2018: Moved Banner Ads to be a part of searched fuel results.  
08-02-2018: Added Login Support. Offline support is only available for favourite stations and searched history.  
06-02-2018: Added Widget support.  
25-01-2018: Removed Station Detail screen. Now details are shown via collapse screen in station list screen.  
18-01-2018: Recent Search list can now be deleted via holding down an item.  
24-12-2017: Station Detail screen can now Share, mark as favourite and get direction to the station (will re-direct to google maps).  
24-12-2017: User preferences now moved to station list screen as actions (Before in Settings).  
13-12-2017: Merged Search and Near Me tabs into one, Favourites tab now functioning.  
24-11-2017: Search tab now support offline use (cache, recently searched location).  
20-11-2017: Added timestamp to show when the data was fetched.  
20-11-2017: NearMe tab now support offline use (cache).  
13-11-2017: Added FRDB cache to support performance.  
27-09-2017: stations are now clickable, showing it's detail.  
14-09-2017: SearchFragment now functional.  
08-09-2017: Add more preferences in Settings. This will allow users to have more control in sorting and radius of values.  
07-09-2017: Added petrol type preferences in Settings and search ability in Search screen.  
04-09-2017: Added distance data in UI.  
28-08-2017: Add more petrol station brand support and allow app to retry fetching when device has regained internet & GPS connection.  
28-08-2017: Fetching Stations is implemented for NearBy section of the app.  

### BUG FIXES:
19-12-2018: Fix app launch from widget click, it was set a separate task so that relaunching from the app drawer wasn't resuming the app.  
19-12-2018: Another fix to history deletion, wasn't indicating to user that there's no history when empty.  
13-12-2018: Minor bug fix in relation to history deletion when offline.  
06-12-2018: Fix rotation issues when offline cache is used.  
06-12-2018: Fix UI issues involving station detail's error message showing up regardless of any errors and a line overlapping the detail/loading circle.  
06-12-2018: Fix UI issue involving results, connection error showing when cache is shown.  
01-11-2018: Fix bug that involves loading loop when finding fuel results with Internet OFF but Location ON.  
01-11-2018: Fix bug that happens in a No internet detected, Location is not turned on or Location service is unavailable during an orientation change when fetching results via user location (crash does not happen in a searched/history selected location).  
01-11-2018: Fix bug in history deletion. Occurs when there are 4 or more items, selection last and 3rd last item for deletion will show 2 items deleted, but the 3rd last item remains there though when clicked, it shows the previous 2nd last item correctly. Going back to the home screen however, shows the 3rd item there and in the end, the 2nd last item that was shown does not exist (Fixed in DB side, but not in App side).  
01-11-2018: Fix bug in history selection. User is to hold down an item long to activate history selection for deletion. However if user keeps on doing the same thing, it'll activate a new action mode, resulting in multiple action modes.  
01-11-2018: Fix bug when location is not turned on, it still fetches the previous data when rotated which isn't suppose to.  
20-08-2018: Fixed issue with retrieving results via device location. The issue involved pressing the "My Location" button twice, with the first press involving asking the user permission.  
20-08-2018: Fixed bug that involved multiple clicks in toolbar buttons (in Home section).  
20-08-2018: Fixed app crash when requesting for results within device's location. (it occurs when gps/location is resetted).  
03-07-2018: Fixed issue when resuming app via app launcher (App was not resuming from app launcher after it was launched via widget intent).  
02-07-2018: Fixed widget data not showing correct station.  
02-03-2018: Revision on previous bug fix. Bug still occured if previously app state has Dialog opened. Fixed.  
01-03-2018: Fixed duplicate station detail dialogs, upon entering from Widget.  
27-02-2018: Fixed Widget data not showing correct fuel data.  
26-02-2018: Fixed app crash that occured on certain number of station results.  
20-02-2018: Utilises cached data for config changes (to provide smooth experience to the user).  
09-02-2018: Fixed ad reloading issue.   
06-02-2018: Favourite stations now syncs with Fuel API.  
24-11-2017: Fixed syncing issues when switching fragments.  
22-11-2017: Fixed app crash when switching fragments.  
22-11-2017: Fixed initial launch issue of fetching data.  
04-09-2017: Fixed sorting of stations/prices issue.  
