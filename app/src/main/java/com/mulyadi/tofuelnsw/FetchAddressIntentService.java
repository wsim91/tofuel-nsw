package com.mulyadi.tofuelnsw;


import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import com.mulyadi.tofuelnsw.Util.Preference;
import com.mulyadi.tofuelnsw.Util.UserPreference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Fetching the address is possible with AsyncTask but reliability is not guarranteed.
 * This is because AsynTask is designed for short operations, and of course,
 * the obvious reason of it being in-efficient when activity is re-created.
 * In contrast, an IntentService doesn't need to be cancelled when the activity is rebuilt.
 * Created by Welly Mulyadi on 21/08/2017.
 */

public class FetchAddressIntentService extends IntentService {

    public static String LOG_TAG = FetchAddressIntentService.class.getSimpleName();
    public static final int FAILURE_RESULT = 1;
    public static final int SUCCESS_RESULT = 0;

    ResultReceiver mReceiver;

    public FetchAddressIntentService(){
        super(LOG_TAG);
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        String errorMessage = "";
        Preference preference = UserPreference.getInstance(getApplicationContext());

        // Get the location passed to this service through an extra.
        Location location = intent.getParcelableExtra(
                preference.getLocationDataKey());

        mReceiver = intent.getParcelableExtra(preference.getAddressReceiverKey());

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    // we just need 1 address i.e. users location
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.location_service_unavailable);
            Log.e(LOG_TAG, errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.lat_long_untranslatable, ""+location.getLatitude(),""+location.getLongitude());
            Log.e(LOG_TAG, errorMessage, illegalArgumentException);
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size()  == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e(LOG_TAG, errorMessage);
            }
            deliverResultToReceiver(FAILURE_RESULT, errorMessage);
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<String>();

            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            for(int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }
            Log.i(LOG_TAG, getString(R.string.address_found));
            deliverResultToReceiver(SUCCESS_RESULT,
                    TextUtils.join(System.getProperty(getString(R.string.line_separator)),
                            addressFragments));
        }
    }

    private void deliverResultToReceiver(int resultCode, String message) {
        Bundle bundle = new Bundle();
        bundle.putString(UserPreference.getInstance(getApplicationContext()).getAddressResultDataKey(), message);
        mReceiver.send(resultCode, bundle);
    }

}
