package com.mulyadi.tofuelnsw.FuelApiRequest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Welly Mulyadi on 23/08/2017.
 */

public class FuelByLocationRequest {

    @SerializedName("fueltype")
    String fuelType;
    @SerializedName("brand")
    String[] brand;
    @SerializedName("latitude")
    double latitude;
    @SerializedName("longitude")
    double longitude;
    @SerializedName("radius")
    int radius;
    @SerializedName("sortby")
    String sortby;
    @SerializedName("sortascending")
    boolean sortAscending;

    public FuelByLocationRequest(String fuelType, String[] brand, double latitude, double longitude,
                                 int radius, String sortby, boolean sortAscending){
        this.fuelType = fuelType;
        this.brand = brand;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
        this.sortby = sortby;
        this.sortAscending = sortAscending;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String[] getBrand() {
        return brand;
    }

    public void setBrand(String[] brand) {
        this.brand = brand;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getSortby() {
        return sortby;
    }

    public void setSortby(String sortby) {
        this.sortby = sortby;
    }

    public boolean isSortAscending() {
        return sortAscending;
    }

    public void setSortAscending(boolean sortAscending) {
        this.sortAscending = sortAscending;
    }
}
