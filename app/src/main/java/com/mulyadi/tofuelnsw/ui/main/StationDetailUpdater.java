package com.mulyadi.tofuelnsw.ui.main;

import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;

/**
 * Created by Welly Mulyadi on 2/02/2018.
 */

public interface StationDetailUpdater {

    void updateStationDetails(AllPetrolPriceByStationResponse results);
    void fetchOfflineData();
}
