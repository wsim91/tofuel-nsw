package com.mulyadi.tofuelnsw.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.Util.Preference;
import com.mulyadi.tofuelnsw.Util.Repository;
import com.mulyadi.tofuelnsw.Util.UserPreference;
import com.mulyadi.tofuelnsw.ui.detail.DialogStationFragment;
import com.mulyadi.tofuelnsw.ui.main.favourite.FavouritesFragment;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Welly Mulyadi on 18/08/2017.
 */
public class MainActivity extends AppCompatActivity implements MainContract.View, DialogStationFragment.OnDeleteStationListener{

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 2;

    /**Tags for fragments**/
    @BindString(R.string.title_home)
    String mHomeTag;
    @BindString(R.string.title_favourites)
    String mFavouritesTag;
    @BindString(R.string.current_visible_fragment)
    String currentFragmentKey;
    @BindString(R.string.favourites_list_position)
    String listPositionKey;

    /**ResultsView bindings**/
    @BindView(R.id.verifyingUserLoadingBar)
    ProgressBar verifyingUserLoadingBar;
    @BindView(R.id.signInAppBar)
    View signInAppBar;
    @BindView(R.id.navigation)
    BottomNavigationView mNavigation;
    @BindView(R.id.viewPager)
    MainViewPager mViewPager;
    @BindView(R.id.networkErrorTextView)
    TextView errorTextView;
    @BindView(R.id.signInAction)
    Button signInAction;

    ViewPagerAdapter mViewPagerAdapter;
    GoogleSignInClient mGoogleSignInClient;
    int fragmentPosition;
    //Since we're checking authentication in onResume(), we're also showing the appropriate fragments here since it depends on authentication.
    //showing the appropriate fragments should only be triggered when the activity is created/re-created, so this boolean helps to achieve that
    boolean isInstantiated;
    int widgetStationCode = -1;
    Preference mPref;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mViewPager.setCurrentItem(ViewPagerAdapter.HOME_FRAGMENT_POSITION,false);
                    fragmentPosition = ViewPagerAdapter.HOME_FRAGMENT_POSITION;
                    return true;
                case R.id.navigation_favourites:
                    mViewPager.setCurrentItem(ViewPagerAdapter.FAVOURITE_FRAGMENT_POSITION,false);
                    fragmentPosition = ViewPagerAdapter.FAVOURITE_FRAGMENT_POSITION;

                    return true;
            }
            return false;
        }

    };
    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPref = UserPreference.getInstance(this);
        ButterKnife.bind(this);
        setUp(savedInstanceState);
        mainPresenter = new MainPresenter(Repository.getMainInstance(this), this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mainPresenter.start();
    }

    @Override
    public void showSignInPrompt() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(mPref.getGoogleSignInIdTokenKey())
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void launchUI() {
        showFragment();
    }

    public void setUp(Bundle savedInstanceState){
        isInstantiated = true;
        fragmentPosition =ViewPagerAdapter.HOME_FRAGMENT_POSITION;

        if (savedInstanceState != null) {
            fragmentPosition = savedInstanceState.getInt(currentFragmentKey, ViewPagerAdapter.HOME_FRAGMENT_POSITION);
        }
        if(getIntent() != null && getIntent().hasExtra(mPref.getWidgetStationCodeKey())){
            fragmentPosition = ViewPagerAdapter.FAVOURITE_FRAGMENT_POSITION;
            widgetStationCode = getIntent().getIntExtra(mPref.getWidgetStationCodeKey(), -1);
            //We need to remove it, otherwise the values still exist and we'll still extract from intent regardless it was from a screen orientation change.
            getIntent().removeExtra(listPositionKey);
            getIntent().removeExtra(currentFragmentKey);
        }
        mNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }



    private void showFragment(){
        if(mViewPagerAdapter == null) {
            Bundle favouriteBundle = null;
            if(widgetStationCode != -1){
                favouriteBundle = new Bundle();
                favouriteBundle.putInt(mPref.getWidgetStationCodeKey(), widgetStationCode);
            }
            mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this, favouriteBundle);
            mViewPager.setAdapter(mViewPagerAdapter);
        }

        mNavigation.setSelectedItemId(
                (fragmentPosition == ViewPagerAdapter.HOME_FRAGMENT_POSITION) ?
                        R.id.navigation_home :
                        R.id.navigation_favourites);
        isInstantiated = false;


        mViewPager.setCurrentItem(fragmentPosition);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        if(intent.hasExtra(mPref.getWidgetStationCodeKey())){
            fragmentPosition = ViewPagerAdapter.FAVOURITE_FRAGMENT_POSITION;
            int stationCode = intent.getIntExtra(mPref.getWidgetStationCodeKey(), -1);
            mViewPagerAdapter.setUpdateStationClick(stationCode);
            mViewPagerAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(currentFragmentKey, fragmentPosition);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RC_SIGN_IN){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                mainPresenter.linkUserToApp(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                mainPresenter.issueError();
            }
        }
    }

    @Override
    public void hideNavBar() {
        mNavigation.setVisibility(View.GONE);
    }

    @Override
    public void showNavBar() {
        mNavigation.setVisibility(View.VISIBLE);
    }

    private View.OnClickListener mSignInOnClickListener = null;
    @Override
    public void hideSignInAction() {
        mSignInOnClickListener = null;
        signInAction.setOnClickListener(null);
        signInAction.setVisibility(View.GONE);
    }

    @Override
    public void showSignInAction() {
        signInAction.setVisibility(View.VISIBLE);
        if(mSignInOnClickListener == null){
            mSignInOnClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainPresenter.refresh();
                }
            };
        }
        signInAction.setOnClickListener(mSignInOnClickListener);
    }

    @Override
    public void hideAppBar() {
        signInAppBar.setVisibility(View.GONE);
    }

    @Override
    public void showAppBar() {
        signInAppBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        verifyingUserLoadingBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        verifyingUserLoadingBar.setVisibility(View.GONE);
    }

    @Override
    public void showSignInFailedError() {
        errorTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSignInFailedError() {
        errorTextView.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        //Since view pager creates both fragment at once, we need to simulate a back stack pop here
        //Though we can do it by adding the fragments to back stack via fragment manager,
        //an easier way would just set the current item back to Home because luckily, we only have 2 fragments
        //So no need to add back to stack
        if(fragmentPosition!=ViewPagerAdapter.HOME_FRAGMENT_POSITION){
            mViewPager.setCurrentItem(ViewPagerAdapter.HOME_FRAGMENT_POSITION, false);
            fragmentPosition = ViewPagerAdapter.HOME_FRAGMENT_POSITION;
            mNavigation.setSelectedItemId(R.id.navigation_home);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void onDeleteFavouriteStation(int positionDeleted) {
        //mViewPager.setCurrentItem(ViewPagerAdapter.FAVOURITE_FRAGMENT_POSITION,false);
        ((FavouritesFragment)mViewPagerAdapter.instantiateItem(mViewPager,ViewPagerAdapter.FAVOURITE_FRAGMENT_POSITION)).deleteFavouriteStation(positionDeleted);
    }

    public BottomNavigationView getMyNavigation(){
        return mNavigation;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
