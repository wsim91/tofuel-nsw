package com.mulyadi.tofuelnsw.ui.main.FuelApiResponse;

import com.google.gson.annotations.SerializedName;
import com.mulyadi.tofuelnsw.Data.StationPrice;
import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.Data.UserSetting;

import java.util.List;

/**
 * Created by Welly Mulyadi on 23/08/2017.
 */

public class FuelByLocationResponse {
    @SerializedName("stations")
    List<Station> stations;
    @SerializedName("prices")
    List<StationPrice> prices;

    UserSetting userSetting;


    //For default empty responses
    public FuelByLocationResponse(List<Station> stations, List<StationPrice> prices){
        setStations(stations);
        setPrices(prices);
    }

    public  FuelByLocationResponse(){
        //required empty constructor for extracting data from FRDB
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;

    }

    public List<StationPrice> getPrices() {
        return prices;
    }

    public void setPrices(List<StationPrice> prices) {
        this.prices = prices;
    }


    public UserSetting getUserSetting() {
        return userSetting;
    }

    public void setUserSetting(UserSetting userSetting) {
        this.userSetting = userSetting;
    }


}
