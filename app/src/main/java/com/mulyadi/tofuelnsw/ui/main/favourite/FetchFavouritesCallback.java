package com.mulyadi.tofuelnsw.ui.main.favourite;

import com.mulyadi.tofuelnsw.Data.Station;

import java.util.List;

/**
 * Created by Welly Mulyadi on 14/03/2018.
 */

public interface FetchFavouritesCallback {
    void onFetchComplete(List<Station> favourites);
}
