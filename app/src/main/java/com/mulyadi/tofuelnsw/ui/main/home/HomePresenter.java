package com.mulyadi.tofuelnsw.ui.main.home;

import com.mulyadi.tofuelnsw.Data.UserLocation;
import com.mulyadi.tofuelnsw.Data.UserSetting;
import com.mulyadi.tofuelnsw.Util.FetchLocationCallback;
import com.mulyadi.tofuelnsw.Util.LocationFinder;
import com.mulyadi.tofuelnsw.Util.PermissionHandler;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.main.AuthenticateUserCallback;

import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.LOCATION_HARDWARE;

/**
 * Created by Welly Mulyadi on 20/03/2018.
 */

public class HomePresenter implements HomeContract.Presenter{

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private static final int UNKNOWN = -1;
    private static final int FETCH_MODE = 0;
    private static final int ACTION_MODE = 1;

    private PermissionHandler mPermissionHandler;
    private HomeRepository mHomeRepository;
    private LocationFinder mLocationFinder;
    private HomeContract.View mHomeView;
    private HomeAuthUserCallback mAuthUserCallback;
    private boolean isHistoryShown;

    public HomePresenter(HomeContract.View view, PermissionHandler permissionHandler, HomeRepository homeRepository, LocationFinder locationFinder) {
        mHomeView = view;
        mPermissionHandler = permissionHandler;
        mHomeRepository = homeRepository;
        mLocationFinder = locationFinder;
        mAuthUserCallback =  new HomeAuthUserCallback();
        isHistoryShown = false;
    }

    @Override
    public void start() {
        if(mHomeRepository.isLocalHistoryDataUpToDate()) {
            if(!isHistoryShown) {
                mHomeView.showHistory(mHomeRepository.getLocalHistory());
                isHistoryShown = true;
            }
            return;
        }
        loadHistory();

    }

    private void loadHistory(){
        mHomeView.hideError();
        mHomeView.showLoading();
        mHomeRepository.setHistoryStatus(false);
        mAuthUserCallback.setFetchMode();
        mHomeRepository.authenticateUser(getClass(), mAuthUserCallback);

    }

    @Override
    public void stop() {
        mHomeRepository.removeAuth(getClass());
    }

    @Override
    public void refresh() {
        loadHistory();
    }

    @Override
    public void deleteHistory(List<Integer> positionsToBeDeleted, HomeFragment.OnHistoryDeleteListener onHistoryDeleteListener) {
        mAuthUserCallback.setActionMode(positionsToBeDeleted, onHistoryDeleteListener);
        mHomeRepository.authenticateUser(getClass(), mAuthUserCallback);
    }

    @Override
    public void searchLocation() {
        mHomeView.showLocationSearch(mHomeRepository.getSearchBound());
    }

    @Override
    public void findMyLocation(boolean alreadyRequested) {
        //no need to authenticate user for this action because all we need is device permission.
        //additionally, results are authenticated when launched
        if(mPermissionHandler.hasPermission(ACCESS_FINE_LOCATION)){
            mHomeView.showResultFindings(null);
        }else{
            if(!alreadyRequested)
                mPermissionHandler.requestPermission(new String[]{ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    private class HomeAuthUserCallback implements AuthenticateUserCallback{

        int mode;
        List<Integer> mToBeDeleted;
        HomeFragment.OnHistoryDeleteListener mOnHistoryDeleteListener;

        public HomeAuthUserCallback() {
            this.mode = UNKNOWN;
            mToBeDeleted = null;
        }

        public void setActionMode(List<Integer> toBeDeleted, HomeFragment.OnHistoryDeleteListener onHistoryDeleteListener){
            mode = ACTION_MODE;
            mToBeDeleted = toBeDeleted;
            mOnHistoryDeleteListener = onHistoryDeleteListener;
        }

        public void setFetchMode(){
            mode = FETCH_MODE;
            mOnHistoryDeleteListener = null;
        }

        @Override
        public void authenticationSuccess() {
            switch(mode){
                case FETCH_MODE:
                    mHomeRepository.fetchHistory(new FetchHistoryCallback() {
                        @Override
                        public void onFetched(List<UserSetting> history) {
                            mHomeView.hideLoading();
                            if(history != null && history.size() > 0) {
                                isHistoryShown = true;
                                mHomeView.showHistory(history);
                            }else{
                                mHomeView.showNoHistoryFoundError();
                            }
                        }

                        @Override
                        public void onFailed() {
                            mHomeView.hideLoading();
                            mHomeView.showHistoryRetrievalError();
                        }
                    });
                    break;
                case ACTION_MODE:
                    if(mToBeDeleted != null && mHomeRepository.isLocalHistoryDataUpToDate() && mOnHistoryDeleteListener != null){
                        List<UserSetting> history = mHomeRepository.getLocalHistory();

                        //List is in reversed. So what we should do is to convert 0th item as last item,
                        //and the conversion for rest will be: last element position minus n-th item
                        int lastElementIndex = history.size() - 1;
                        for(int ix = 0; ix < mToBeDeleted.size(); ix ++){
                            if(mToBeDeleted.get(ix) == 0){
                                history.remove(lastElementIndex);
                            }else{
                                history.remove(lastElementIndex - mToBeDeleted.get(ix));
                            }
                        }
                        mHomeRepository.updateHistory();
                        mOnHistoryDeleteListener.onHistoryDeletedSuccess(history.isEmpty());
                        mToBeDeleted = null;
                    }
                    break;

            }
        }

        @Override
        public void authenticationFailure() {
           stop();
        }
    }
}
