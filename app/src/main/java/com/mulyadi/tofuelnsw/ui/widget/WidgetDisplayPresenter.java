package com.mulyadi.tofuelnsw.ui.widget;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.ui.main.DataFetchedEventListener;
import com.mulyadi.tofuelnsw.ui.main.favourite.FavouritesRepository;
import com.mulyadi.tofuelnsw.ui.main.favourite.FetchFavouritesCallback;
import com.mulyadi.tofuelnsw.ui.widget.WidgetContract.WidgetDisplayContract.*;

import java.util.List;

public class WidgetDisplayPresenter implements Presenter, FetchFavouritesCallback{

    FavouritesRepository mFavRepository;
    FavouriteWidgetRepository mWidgetRepository;
    View mWidgetView;
    String mStationAddress;
    int mAppWidgetId;

    public WidgetDisplayPresenter(FavouritesRepository favRepository, FavouriteWidgetRepository widgetRepository, View widgetView, String stationAddress, int appWidgetId) {
        mFavRepository = favRepository;
        mWidgetRepository = widgetRepository;
        mWidgetView = widgetView;
        mStationAddress = stationAddress;
        mAppWidgetId = appWidgetId;
    }

    @Override
    public void start() {
        mWidgetView.showLoading(mAppWidgetId);
        mFavRepository.fetchFavourites(WidgetDisplayPresenter.this);
    }

    @Override
    public void stop() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onFetchComplete( List<Station> favourites) {
        for (final Station station : favourites) {
            if (station.getAddress().equals(mStationAddress)) {
                mWidgetRepository.fetchWidgetDetails(station, new DataFetchedEventListener() {
                    @Override
                    public void onSuccessDataFetched(Object result) {
                        if (result != null && result instanceof AllPetrolPriceByStationResponse) {
                            mWidgetView.showStationDetail(mAppWidgetId, (AllPetrolPriceByStationResponse)result, station);
                        }
                    }

                    @Override
                    public void onFailDataFetched(int errCode) {
                      mWidgetView.showError(mAppWidgetId, errCode, station);
                    }
                });
                break;
            }
        }
    }


}
