package com.mulyadi.tofuelnsw.ui.main.favourite;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.Util.Preference;
import com.mulyadi.tofuelnsw.Util.UserPreference;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.detail.DialogStationFragment;
import com.mulyadi.tofuelnsw.Util.Repository;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;


public class FavouritesFragment extends Fragment implements FavouritesContract.View {


    final String LOG_TAG = FavouritesFragment.class.getSimpleName();

    @BindString(R.string.favourites_list_position)
    String listPositionKey;
    @BindString(R.string.detail_source)
    String detailSourceKey;
    @BindString(R.string.title_favourites)
    String fragmentTitleStr;
    @BindString(R.string.selectedStation)
    String selectedStationKey;
    @BindString(R.string.toast_delete_favourite_success)
    String deleteSuccessToastMsg;

    @BindString(R.string.dialog_station_detail_tag)
    String dialogTag;
    int listPosition = 0;
    int stationCode;
    @BindView(R.id.favRecyclerView)
    RecyclerView favRecyclerView;
    @BindView(R.id.noFavFoundTextView)
    TextView noFavFoundTextView;
    @BindView(R.id.favCoordLayout)
    CoordinatorLayout favCoordLayout;

    FavouriteStationsAdapter favouriteStationsAdapter;
    LinearLayoutManager mLinearLayoutManager;

    FavouritePresenter mFavPresenter;
    Preference mPref;
    boolean sourceWidgetClickFragmentExist = false;


    public FavouritesFragment() {
        // Required empty public constructor
    }

    ;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_favourites, container, false);
        ButterKnife.bind(this, view);
        mLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        favouriteStationsAdapter = new FavouriteStationsAdapter(getContext(), new ArrayList<Station>(), mFavouriteItemListener);
        mFavPresenter = new FavouritePresenter(this, Repository.getFavouritesInstance(getContext()));
        favRecyclerView.setLayoutManager(mLinearLayoutManager);
        favRecyclerView.setAdapter(favouriteStationsAdapter);
        mPref = UserPreference.getInstance(getContext());

        if(savedInstanceState != null){
            listPosition = savedInstanceState.getInt(listPositionKey, 0);
            savedInstanceState.remove(listPositionKey);
        }

        if(getArguments() != null){
            stationCode = getArguments().getInt(mPref.getWidgetStationCodeKey(), -1);
            getArguments().remove(mPref.getWidgetStationCodeKey());
        }



        return view;
    }



    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt(listPositionKey, mLinearLayoutManager.findFirstVisibleItemPosition());
        super.onSaveInstanceState(outState);

    }

    public void passOnStationCode(int stationCode){
        mFavPresenter.resetIsShown();
        this.stationCode = stationCode;
        mFavPresenter.start();
    }

    @Override
    public void onStart() {
        super.onStart();
       mFavPresenter.start();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mFavPresenter != null) {
            mFavPresenter.stop();
            mFavPresenter = null;
        }
    }

    @Override
    public void showFavourites(List<Station> favourites) {
        favouriteStationsAdapter.addStations(favourites);
        int stationPositionInList = mFavPresenter.findStationPositionInList(stationCode);
        if(stationPositionInList != -1){
            listPosition = stationPositionInList;
            mFavPresenter.openFavouriteDetail(stationPositionInList);
            stationCode = -1;
        }
        favRecyclerView.scrollToPosition(listPosition);


    }

    @Override
    public void showNoFavouritesFoundError() {

    }

    @Override
    public void showDetailDialog(Station station) {
        Fragment fragment = getChildFragmentManager().findFragmentByTag(dialogTag);
        if (fragment != null && fragment instanceof DialogStationFragment) {
            ((DialogStationFragment) fragment).dismiss();
        }
        DialogStationFragment detailFragment = new DialogStationFragment();
        Bundle detailBundle = new Bundle();
        detailBundle.putString(detailSourceKey, fragmentTitleStr);
        detailBundle.putParcelable(selectedStationKey, station);
        detailFragment.setArguments(detailBundle);
        detailFragment.show(getChildFragmentManager(), dialogTag);
    }

    FavouriteItemListener mFavouriteItemListener = new FavouriteItemListener() {
        @Override
        public void onStationClick(int stationPos) {
         mFavPresenter.openFavouriteDetail(stationPos);
        }
    };



    public void deleteFavouriteStation(int positionDeleted) {
        favouriteStationsAdapter.removeStationInPosition(positionDeleted);
    }


    public class FavouriteStationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        static final int FAVOURITE_STATION_VIEWTYPE = 0;
        private final int LIST_ITEM_FUEL_LAYOUT_ID = R.layout.fuel_list_item;
        List<Station> favouriteStations;
        private Context mContext;
        private FavouriteItemListener mFavouriteItemListener;

        public FavouriteStationsAdapter(Context context, ArrayList<Station> favouriteStations,FavouriteItemListener favouriteItemListener) {
            super();
            this.favouriteStations = favouriteStations;
            mContext = context;
            mFavouriteItemListener = favouriteItemListener;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            return new FavouriteStationHolder(LayoutInflater.from(parent.getContext()).inflate(LIST_ITEM_FUEL_LAYOUT_ID, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            final FavouriteStationHolder favouriteStationsViewHolder = (FavouriteStationHolder) holder;

            favouriteStationsViewHolder.distTextView.setVisibility(View.GONE);
            favouriteStationsViewHolder.stationNameTextView.setText(favouriteStations.get(position).getName());
            favouriteStationsViewHolder.stationAddressTextView.setText(favouriteStations.get(position).getAddress());


            int logoResource = Util.getStationDrawableResource(favouriteStations.get(position).getBrand(), mContext);
            favouriteStationsViewHolder.stationBrandImageView.setImageResource(logoResource);

            favouriteStationsViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mFavouriteItemListener.onStationClick(favouriteStationsViewHolder.getAdapterPosition());
                }
            });

        }

        public void addStations(List<Station> stations) {
            favouriteStations = stations;
            notifyDataSetChanged();
        }

        public void removeStationInPosition(int position) {
            if (position >= 0 && favouriteStations.size() > 0) {
                favouriteStations.remove(position);
                notifyItemRemoved(position);
            }

        }

        @Override
        public int getItemViewType(int position) {
            return FAVOURITE_STATION_VIEWTYPE;
        }

        @Override
        public int getItemCount() {
            return favouriteStations.size();
        }

        public void reset() {
            favouriteStations.clear();
            notifyDataSetChanged();
        }

        public class FavouriteStationHolder extends RecyclerView.ViewHolder {


            private final String LOG_TAG = FavouriteStationHolder.class.getSimpleName();

            @BindView(R.id.stationBrandImageView)
            ImageView stationBrandImageView;
            @BindView(R.id.stationNameTextView)
            TextView stationNameTextView;
            @BindView(R.id.bottomDivider)
            View bottomDivider;
            @BindView(R.id.stationAddressTextView)
            TextView stationAddressTextView;

            @BindView(R.id.distTextView)
            TextView distTextView;

            View rootView;


            //As mentioned in Google IO 2016, it is crucial to use the whole view for the expanding/collapsing animation
            //to do so, it requires a ViewGroup
            public FavouriteStationHolder(View rootView) {
                super(rootView);

                ButterKnife.bind(this, rootView);
                this.rootView = rootView;
            }

        }


    }

    interface FavouriteItemListener{
        void onStationClick(int stationPos);
    }
}
