package com.mulyadi.tofuelnsw.ui.detail;

import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.main.StationDetailUpdater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mulya on 20/09/2017.
 */

public class FetchAllPricesForStationCallback implements Callback<AllPetrolPriceByStationResponse> {

    private static final String LOG_TAG = FetchAllPricesForStationCallback.class.getSimpleName();
    private StationDetailUpdater mUpdater;
    private String mPrefFuel;

    public FetchAllPricesForStationCallback(StationDetailUpdater updater, String prefFuel){
        mUpdater = updater;
        mPrefFuel = prefFuel;
    }
    @Override
    public void onResponse(Call<AllPetrolPriceByStationResponse> call, Response<AllPetrolPriceByStationResponse> response) {
        Util.logDebug(LOG_TAG, call.request().toString());
        AllPetrolPriceByStationResponse result = response.body();
        //moves the preferred to the top
        if(result != null) {
            for (int ix = 0; ix < result.getPrices().size(); ix++) {
                if (result.getPrices().get(ix).getFuelType().equals(mPrefFuel)) {
                    result.getPrices().add(0, result.getPrices().remove(ix));
                    break;
                }
            }
        }else{
            mUpdater.fetchOfflineData();
        }

        mUpdater.updateStationDetails(result);
    }

    @Override
    public void onFailure(Call<AllPetrolPriceByStationResponse> call, Throwable t) {
        mUpdater.fetchOfflineData();
    }
}
