package com.mulyadi.tofuelnsw.ui.main;



/**
 * Created by Welly Mulyadi on 8/02/2018.
 */

public interface DataFetchedEventListener {

    void onSuccessDataFetched(Object result);
    void onFailDataFetched(int errCode);
}
