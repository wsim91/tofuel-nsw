package com.mulyadi.tofuelnsw.ui.detail;


/**
 * Created by Welly Mulyadi on 5/03/2018.
 */

public interface FavouriteStatusListener {
    void onStatusChecked(boolean isFavourite);
}