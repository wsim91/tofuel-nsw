package com.mulyadi.tofuelnsw.ui.detail;

import com.mulyadi.tofuelnsw.Util.DialogCallback;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.ui.main.AuthenticateUserCallback;
import com.mulyadi.tofuelnsw.ui.main.DataFetchedEventListener;

import static com.mulyadi.tofuelnsw.Rest.FuelApiClient.NETWORK_ERROR_CODE;

/**
 * Created by Welly Mulyadi on 5/03/2018.
 */

public class DetailPresenter implements FetchDetailContract.Presenter, DataFetchedEventListener{

    private static final String LOG_TAG = DetailPresenter.class.getSimpleName();

    private DetailRepository mDetailRepository;
    private boolean isFavMode;
    private FetchDetailContract.View mDetailView;
    private String stationName;



    public DetailPresenter(boolean isFavMode, FetchDetailContract.View mDetailView, DetailRepository mDetailRepository) {
        this.mDetailRepository = mDetailRepository;
        this.isFavMode = isFavMode;
        this.mDetailView = mDetailView;
        stationName = mDetailRepository.fetchStation().getName();
    }

    @Override
    public void stop() {
        mDetailRepository.removeAuth(getClass());
    }

    @Override
    public void start() {
        hideAllErrors();
        mDetailRepository.authenticateUser(getClass(),new AuthenticateUserCallback() {
            @Override
            public void authenticationSuccess() {
                if(mDetailRepository.getDetails() == null) {
                    mDetailView.showContentLoading();
                    mDetailRepository.fetchDetails(DetailPresenter.this);
                }else{
                    mDetailView.hideContentLoading();
                    mDetailView.addBasicStationInfo(mDetailRepository.fetchStation());
                    mDetailView.addDetail(mDetailRepository.getDetails());
                    showActionMode();
                    markStationAsFav(mDetailRepository.getIsStationMarked());

                }
            }

            @Override
            public void authenticationFailure() {
                mDetailView.showAuthenticationError();
            }
        });


    }

    @Override
    public void refresh() {

    }

    private void hideAllErrors(){
        mDetailView.hideConnectivityError();
        mDetailView.hideServerError();
        mDetailView.hideAuthenticationError();
    }

    @Override
    public void onSuccessDataFetched(final Object result) {
        mDetailRepository.fetchFavouriteStatus(new FavouriteStatusListener() {
            @Override
            public void onStatusChecked(boolean isFavourite) {
                mDetailView.hideFavouriteLoading();
                showActionMode();
                markStationAsFav(isFavourite);

                if(result != null) {
                    mDetailView.hideContentLoading();
                    mDetailView.addDetail((AllPetrolPriceByStationResponse) result);
                    mDetailView.addBasicStationInfo(mDetailRepository.fetchStation());
                }
            }
        });
    }
    @Override
    public void onFailDataFetched(int errCode) {
        mDetailView.hideContentLoading();
        if(errCode == NETWORK_ERROR_CODE){
            mDetailView.showConnectivityError();
        }else{
            mDetailView.showServerError();
        }
    }

    @Override
    public void deleteStation() {
        mDetailView.showDeleteConfirmation(mDetailRepository.fetchStation().getName(), new DialogCallback() {
            @Override
            public void confirmedClicked() {
                mDetailRepository.deleteStation(new StationDeleteCallback() {
                    @Override
                    public void deleteComplete(String stationName, int positionToBeDeleted) {
                        mDetailView.showDeleteSuccess(stationName);
                        mDetailView.notifyDeleteObserver(positionToBeDeleted);
                    }

                    @Override
                    public void deleteFailed() {
                        mDetailView.showDeleteFailed();
                    }
                });
            }
        });
    }

    private void showActionMode(){
        if(isFavMode){
            mDetailView.hideResultMode();
            mDetailView.showFavouriteMode();
        }else{
            mDetailView.hideFavouriteMode();
            mDetailView.showResultMode();
        }
    }

    @Override
    public void toggleFavouriteStatus() {
        mDetailView.showFavouriteLoading();
        mDetailView.disableMarkingFavourite();
        mDetailRepository.switchFavouriteStatus(new FavouriteStatusListener() {
            @Override
            public void onStatusChecked(boolean isFavourite) {
                mDetailView.hideFavouriteLoading();
                markStationAsFav(isFavourite);
            }
        });
    }

    @Override
    public void getStationInformation() {
        mDetailView.shareStationInformation(mDetailRepository.getShareInformation());
    }

    @Override
    public void getDirection() {
        mDetailView.showDirection(mDetailRepository.getStationMapURI());
    }


    private void markStationAsFav(boolean mark){
        mDetailView.setFadeAnimationToFavMarking(true);
        if(mark){
            mDetailView.showFavouriteMarked(stationName);
        }else{
            mDetailView.showFavouriteUnmarked(stationName);
        }

        mDetailView.enableMarkingFavourite();
    }

    @Override
    public void setUpShareFunctionality() {
        mDetailView.setUpShare(stationName);
    }

    @Override
    public void setUpDirectionFunctionality() {
        mDetailView.setUpDirection(stationName);
    }

    @Override
    public void setUpFavOrDeleteFunctionality(boolean isFavMode) {
        if(isFavMode){
            mDetailView.setUpDelete(stationName);
        }else{
            mDetailView.setUpFavourite();
        }
    }
}
