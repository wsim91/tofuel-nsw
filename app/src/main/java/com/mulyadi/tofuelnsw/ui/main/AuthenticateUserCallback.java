package com.mulyadi.tofuelnsw.ui.main;

/**
 * Created by Welly Mulyadi on 2/03/2018.
 */

public interface AuthenticateUserCallback {
    void authenticationSuccess();
    void authenticationFailure();
}
