package com.mulyadi.tofuelnsw.ui.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.Util.Preference;
import com.mulyadi.tofuelnsw.Util.Repository;
import com.mulyadi.tofuelnsw.Util.UserPreference;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.ui.main.MainActivity;
import com.mulyadi.tofuelnsw.ui.widget.WidgetContract.WidgetDisplayContract.*;

import static android.appwidget.AppWidgetManager.ACTION_APPWIDGET_UPDATE;
import static com.mulyadi.tofuelnsw.Rest.FuelApiClient.DATA_UNAVAILABLE_ERROR_CODE;
import static com.mulyadi.tofuelnsw.Rest.FuelApiClient.INTERNAL_SERVER_ERROR_CODE;
import static com.mulyadi.tofuelnsw.Rest.FuelApiClient.NETWORK_ERROR_CODE;


/**
 * Created by Welly Mulyadi on 30/01/2018.
 */

public class FavouriteStationWidgetProvider extends AppWidgetProvider{

    static final String LOG_TAG = FavouriteStationWidgetProvider.class.getSimpleName();



    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            UserPreference.getInstance(context).deleteWidgetConfig(appWidgetId);
        }
    }

    static void updateWidget(final Context context, final AppWidgetManager appWidgetManager, final int appWidgetId) {
        final String stationAddress = UserPreference.getInstance(context).loadWidgetConfig(appWidgetId);
        if(stationAddress != null) {
            new WidgetDisplayPresenter(Repository.getFavouritesInstance(context),
                    Repository.getWidgetInstance(context),
                    new View() {
                        @Override
                        public void showLoading(int appWidgetId) {
                            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.favourite_station_widget_loading);
                            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
                        }


                        @Override
                        public void showError(int appWidgetId, int errCode, Station station) {
                            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.favourite_station_widget_error);
                            Preference pref = UserPreference.getInstance(context);

                            remoteViews.setViewVisibility(R.id.widgetInternalServerErrorTextView, android.view.View.GONE);
                            remoteViews.setViewVisibility(R.id.widgetNetworkErrorTextView, android.view.View.GONE);
                            remoteViews.setViewVisibility(R.id.widgetTrafficExceedErrorTextView, android.view.View.GONE);
                            remoteViews.setViewVisibility(R.id.widgetNoDataErrorTextView, android.view.View.GONE);

                            int errorTextViewId;

                            switch(errCode){
                                case DATA_UNAVAILABLE_ERROR_CODE:
                                    errorTextViewId = R.id.widgetNoDataErrorTextView;
                                    remoteViews.setTextViewText(R.id.widgetNoDataErrorTextView, pref.getWidgetEmptyDataString(station.getName()));
                                    break;
                                case NETWORK_ERROR_CODE:
                                    errorTextViewId = R.id.widgetNetworkErrorTextView;
                                    break;
                                case INTERNAL_SERVER_ERROR_CODE:
                                    errorTextViewId = R.id.widgetInternalServerErrorTextView;
                                    break;
                                default:
                                    errorTextViewId = R.id.widgetTrafficExceedErrorTextView;
                            }

                            remoteViews.setViewVisibility(errorTextViewId, android.view.View.VISIBLE);

                            Intent refreshIntent = new Intent(context, FavouriteStationWidgetProvider.class);
                            refreshIntent.setAction(ACTION_APPWIDGET_UPDATE);
                            refreshIntent.putExtra(pref.getWidgetIdKey(), appWidgetId);
                            remoteViews.setOnClickPendingIntent(R.id.widget, PendingIntent.getBroadcast(context, appWidgetId, refreshIntent, 0));
                            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
                        }

                        @Override
                        public void showStationDetail(int appWidgetId, AllPetrolPriceByStationResponse detail, Station station) {
                            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.favourite_station_widget);
                            UserPreference pref = UserPreference.getInstance(context);
                            pref.saveWidgetLastUpdatedTime(appWidgetId, detail.getTimeAccessed());
                            views.setTextViewText(R.id.widgetStationNameTextView, station.getName());
                            views.setTextViewText(R.id.widgetLastUpdated, Util.getDateFriendly(detail.getTimeAccessed()));
                            views.setTextViewText(R.id.widgetFuelPriceTextView, "" + detail.getPrices().get(0).getPrice());
                            views.setTextViewText(R.id.widgetFuelTypeTextView, detail.getPrices().get(0).getFuelType());
                            int logoResource = Util.getStationDrawableResource(station.getBrand(), context);
                            views.setImageViewResource(R.id.widgetStationImageView, logoResource);
                            Util.logDebug(LOG_TAG, station.getName());
                            Intent activityIntent = new Intent(context, MainActivity.class);
                            //Pending intents without a unique action, will be updated. This will be an issue when there exists multiple widgets.
                            //To avoid this issue, the action needs to be unique. Since our appwidgetIds are already unique, we'll use it.
                            activityIntent.setAction(""+appWidgetId);
                            activityIntent.putExtra(pref.getWidgetStationCodeKey(), station.getCode());
                            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            views.setOnClickPendingIntent(R.id.widget, PendingIntent.getActivity(context, 0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT));
                            appWidgetManager.updateAppWidget(appWidgetId, views);
                        }
                    }, stationAddress, appWidgetId).start();
        }
    }

    /**
     *In addition to the periodic updates, onReceive() is also triggered when a widget reload occurs, which originated from an issue with loading data(no network connectivity or API issues).
     * The reload happens when user taps the issued widget.
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Preference preference = UserPreference.getInstance(context);
        if(intent.getAction() != null && intent.getAction().equals(ACTION_APPWIDGET_UPDATE)&& intent.hasExtra(preference.getWidgetIdKey())){

            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            updateWidget(context, appWidgetManager,intent.getIntExtra(preference.getWidgetIdKey(), -1));

        }
        else{
            super.onReceive(context, intent);
        }

    }
}
