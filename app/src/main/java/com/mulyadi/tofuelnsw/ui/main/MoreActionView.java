package com.mulyadi.tofuelnsw.ui.main;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

import com.mulyadi.tofuelnsw.R;

/**
 * Created by Welly Mulyadi on 13/12/2017.
 */

public class MoreActionView extends AppCompatImageButton{


    public MoreActionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setImageResource(R.drawable.ic_more_vert);
    }


}
