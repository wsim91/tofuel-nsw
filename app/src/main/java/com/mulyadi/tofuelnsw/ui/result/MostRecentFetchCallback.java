package com.mulyadi.tofuelnsw.ui.result;

import com.mulyadi.tofuelnsw.ui.main.FuelApiResponse.FuelByLocationResponse;

/**
 * Created by Welly Mulyadi on 2/03/2018.
 */

public interface MostRecentFetchCallback {
    void onMostRecentFetched(FuelByLocationResponse mostRecentResult);
}
