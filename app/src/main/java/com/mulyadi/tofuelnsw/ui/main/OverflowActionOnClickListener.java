package com.mulyadi.tofuelnsw.ui.main;

import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

import com.mulyadi.tofuelnsw.R;

/**
 * Created by Welly Mulyadi on 14/12/2017.
 */

public  class OverflowActionOnClickListener implements View.OnClickListener {

    private PopupMenu popupMenu;
    private MenuItem prefItemFuelType;
    private MenuItem prefItemRadius;

    public OverflowActionOnClickListener(MoreActionView moreActionView) {
        popupMenu = new PopupMenu(moreActionView.getContext(), moreActionView);
        popupMenu.inflate(R.menu.tofuelnsw_menu);
        SubMenu fuelTypeMenu = popupMenu.getMenu().getItem(0).getSubMenu();
        SubMenu radiusMenu = popupMenu.getMenu().getItem(1).getSubMenu();

        int prefFuelTypeIndex = PreferenceManager
                .getDefaultSharedPreferences(moreActionView.getContext())
                .getInt(moreActionView.getContext().getString(R.string.fueltype_key), 0);

        int prefRadiusIndex = PreferenceManager
                .getDefaultSharedPreferences(moreActionView.getContext())
                .getInt(moreActionView.getContext().getString(R.string.radius_key), 0);

        prefItemFuelType = fuelTypeMenu.getItem(prefFuelTypeIndex).setIcon(R.drawable.ic_check_black);
        prefItemRadius = radiusMenu.getItem(prefRadiusIndex).setIcon(R.drawable.ic_check_black);


    }

    public PopupMenu getPopupMenu() {
        return popupMenu;
    }

    public MenuItem getPrefItemFuelType() {
        return prefItemFuelType;
    }

    public void setPrefItemFuelType(MenuItem prefItemFuelType) {
        this.prefItemFuelType = prefItemFuelType;
    }

    public MenuItem getPrefItemRadius() {
        return prefItemRadius;
    }

    public void setPrefItemRadius(MenuItem prefItemRadius) {
        this.prefItemRadius = prefItemRadius;
    }

    @Override
    public void onClick(View view) {
        popupMenu.show();
    }
}
