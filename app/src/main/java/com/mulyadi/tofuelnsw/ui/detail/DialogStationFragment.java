package com.mulyadi.tofuelnsw.ui.detail;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.ShareCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mulyadi.tofuelnsw.Data.Price;
import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.Util.DialogCallback;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.Util.Repository;

import java.util.ArrayList;

import butterknife.BindInt;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Welly Mulyadi on 5/03/2018.
 */
public class DialogStationFragment extends DialogFragment implements FetchDetailContract.View {


    static final String LOG_TAG = DialogStationFragment.class.getSimpleName();

    @BindView(R.id.dialogDetailView)
    android.view.View dialogDetailView;
    @BindView(R.id.dividerTop)
    View dividerTop;
    @BindView(R.id.dividerBottom)
    View dividerBottom;
    @BindView(R.id.stationDetailTitle)
    TextView stationDetailTitleView;
    @BindView(R.id.stationDetailDistance)
    TextView stationDetailDistanceView;
    @BindView(R.id.stationAddressTextView)
    TextView stationAddressTextView;
    @BindView(R.id.fuelDetailRecView)
    RecyclerView fuelDetailRecView;
    @BindView(R.id.delFavBtn)
    ImageButton delFavBtn;
    @BindView(R.id.favBtn)
    ImageButton favBtn;
    @BindView(R.id.favouritesMarkProgressBar)
    ProgressBar favouritesMarkProgressBar;
    @BindView(R.id.dataLoadingBar)
    ProgressBar dataLoadingBar;
    @BindView(R.id.directionBtn)
    ImageButton directionBtn;
    @BindView(R.id.shareBtn)
    ImageButton shareBtn;
    @BindView(R.id.errorDialogTextView)
    TextView errorDialogTextView;
    @BindView(R.id.authenticationErrorTextView)
    TextView authenticatrionErrorTextView;
    @BindView(R.id.serverErrorTextView)
    TextView serverErrorTextView;
    @BindView(R.id.clientErrorTextView)
    TextView clientErrorTextView;

    @BindString(R.string.selectedStation)
    String selectedStationKey;
    @BindString(R.string.distance_string)
    String distanceString;
    @BindString(R.string.station_fuel_details)
    String stationFuelDetailsKey;
    @BindString(R.string.detail_source)
    String detailSourceKey;
    @BindString(R.string.title_results)
    String resultFragmentStr;
    @BindString(R.string.dialog_string_delete_favourite)
    String dialogMsg;
    @BindString(R.string.toast_delete_favourite_success)
    String successDeletionMsg;
    @BindString(R.string.toast_delete_favourite_error)
    String failDeletionMsg;
    @BindString(R.string.google_map_package_name)
    String googleMapPackageNameStr;
    @BindString(R.string.no_google_map_apps_installed_error)
    String noGMapInstalledErrorStr;

    //Content Description
    @BindString(R.string.delete_favourite_content_description)
    String deleteFavouriteContentDescription;
    @BindString(R.string.mark_favourite_content_description)
    String markFavouriteContentDescription;
    @BindString(R.string.unmark_favourite_content_description)
    String unmarkFavouriteContentDescription;
    @BindString(R.string.favourites_status_loading_content_description)
    String favouritesStatusLoadingContentDescription;
    @BindString(R.string.share_station_content_description)
    String shareStationContentDescription;
    @BindString(R.string.station_direction_content_description)
    String stationDirectionContentDescription;

    @BindInt(R.integer.dialog_fuel_list_column)
    int noOfColumns;

    DetailPresenter mStationDetailPresenter;
    DetailRecViewAdapter detailRecViewAdapter;



    private OnDeleteStationListener mOnDeleteStationListener = null;

    public DialogStationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //This is needed so the previous activity is triggered for an update if there was a deletion occuring
        if(context instanceof OnDeleteStationListener){
            mOnDeleteStationListener = (OnDeleteStationListener)context;
        }else{
            throw new RuntimeException(context.toString()
                    + getString(R.string.runtime_error_ondeletestationlistener));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnDeleteStationListener = null;
    }

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        android.view.View view = inflater.inflate(R.layout.dialog_detail_station_fragment, container, false);
        ButterKnife.bind(this, view);

        if(getArguments() != null && getArguments().getParcelable(selectedStationKey) != null) {
            Station selectedStation = getArguments().getParcelable(selectedStationKey);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), noOfColumns);
            detailRecViewAdapter = new DetailRecViewAdapter(getContext(),
                    new AllPetrolPriceByStationResponse(new ArrayList<Price>()));
            fuelDetailRecView.setAdapter(detailRecViewAdapter);
            fuelDetailRecView.setLayoutManager(gridLayoutManager);


            boolean isFavMode = getArguments().containsKey(detailSourceKey) && !getArguments().getString(detailSourceKey).equals(resultFragmentStr);
            mStationDetailPresenter = new DetailPresenter(isFavMode,
                    this, Repository.getDetailInstance(getContext(), selectedStation));

           mStationDetailPresenter.setUpFavOrDeleteFunctionality(isFavMode);
           mStationDetailPresenter.setUpDirectionFunctionality();
           mStationDetailPresenter.setUpShareFunctionality();
        }


        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setFadeAnimationToFavMarking(boolean fadeIn){
        Animation anim;
        if(fadeIn){
            anim = new AlphaAnimation(0, 1);
            anim.setInterpolator(new DecelerateInterpolator());
        }else{
            anim = new AlphaAnimation(1, 0);
            anim.setInterpolator(new AccelerateInterpolator());
        }


        anim.setDuration(500);
        favBtn.setAnimation(anim);
    }
    @Override
    public void onResume() {
        super.onResume();
        mStationDetailPresenter.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        mStationDetailPresenter.stop();
    }

    private void delayDismissalOfView(final android.view.View v){

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                        v.setVisibility(android.view.View.GONE);
                    } catch (InterruptedException e) {

                    }
                }
            }).run();
    }
    public interface OnDeleteStationListener{
        void onDeleteFavouriteStation(int positionDeleted);
    }

    @Override
    public void addDetail(AllPetrolPriceByStationResponse allPetrolPriceByStationResponse) {
        detailRecViewAdapter.updateStationDetails(allPetrolPriceByStationResponse);
    }

    @Override
    public void disableMarkingFavourite() {
        favBtn.setEnabled(false);
        favBtn.setClickable(false);
    }

    @Override
    public void enableMarkingFavourite() {
        favBtn.setEnabled(true);
        favBtn.setClickable(true);
    }

    @Override
    public void showContentLoading() {
        dataLoadingBar.setVisibility(android.view.View.VISIBLE);
        dividerTop.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideContentLoading() {
        dataLoadingBar.setVisibility(android.view.View.GONE);
        dividerTop.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFavouriteLoading() {
        favouritesMarkProgressBar.setVisibility(android.view.View.VISIBLE);
        favouritesMarkProgressBar.setContentDescription(favouritesStatusLoadingContentDescription);
    }

    @Override
    public void hideFavouriteLoading() {
        delayDismissalOfView(favouritesMarkProgressBar);
    }

    @Override
    public void showConnectivityError() {
        clientErrorTextView.setVisibility(View.VISIBLE);
        dividerTop.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideConnectivityError() {
        clientErrorTextView.setVisibility(View.GONE);
    }

    @Override
    public void showFavouriteMode() {
        delFavBtn.setVisibility(android.view.View.VISIBLE);
        showBasicInfo();
        showBasicAction();
    }

    @Override
    public void hideFavouriteMode() {
        delFavBtn.setVisibility(android.view.View.GONE);
    }

    @Override
    public void showResultMode() {
        favBtn.setVisibility(android.view.View.VISIBLE);
        stationDetailDistanceView.setVisibility(android.view.View.VISIBLE);
        showBasicInfo();
        showBasicAction();
    }

    @Override
    public void hideResultMode() {
        favBtn.setVisibility(android.view.View.GONE);
        stationDetailDistanceView.setVisibility(android.view.View.GONE);
    }

    private void showBasicInfo(){
        stationAddressTextView.setVisibility(android.view.View.VISIBLE);
        stationDetailTitleView.setVisibility(android.view.View.VISIBLE);
    }

    private void showBasicAction(){
        shareBtn.setVisibility(android.view.View.VISIBLE);
        directionBtn.setVisibility(android.view.View.VISIBLE);
    }
    @Override
    public void showServerError() {
        serverErrorTextView.setVisibility(View.VISIBLE);
        dividerTop.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideServerError() {
        serverErrorTextView.setVisibility(View.GONE);
    }

    @Override
    public void showAuthenticationError() {
        authenticatrionErrorTextView.setVisibility(View.VISIBLE);
        dividerTop.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideAuthenticationError() {
        authenticatrionErrorTextView.setVisibility(View.GONE);
    }

    @Override
    public void showFavouriteMarked(String stationName) {
        favBtn.setImageResource(R.drawable.ic_unsave_from_favourites);
        favBtn.setContentDescription(String.format(unmarkFavouriteContentDescription, stationName));
    }

    @Override
    public void showFavouriteUnmarked(String stationName) {
        favBtn.setImageResource(R.drawable.ic_save_to_favourites);
        favBtn.setContentDescription(String.format(markFavouriteContentDescription, stationName));
    }

    @Override
    public void addBasicStationInfo(Station station) {
        stationDetailTitleView.setText(station.getName());
        stationDetailDistanceView.setText(String.format(distanceString, station.getLocation().getDistance()));
        stationAddressTextView.setText(station.getAddress());
    }


    @Override
    public void shareStationInformation(String sharedInformationStr) {
        if(getActivity() != null){
            getActivity().startActivity(Intent.createChooser(ShareCompat.IntentBuilder.from(getActivity())
                    .setType(getString(R.string.content_text_plain))
                    .setText(sharedInformationStr)
                    .getIntent(),getString(R.string.title_share)));
        }
    }

    @Override
    public void showDirection(Uri geoStationURI) {
        if (getActivity() != null) {

            Intent mapIntent = new Intent(Intent.ACTION_VIEW,geoStationURI);
            mapIntent.setPackage(googleMapPackageNameStr);
            if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                getActivity().startActivity(mapIntent);
            } else {
                Util.logDebug(LOG_TAG, String.format(noGMapInstalledErrorStr, geoStationURI.toString()));
            }
        }
    }

    @Override
    public void showDeleteConfirmation(String stationName, DialogCallback dialogCallback) {
        Util.showDialog(getContext(),
                String.format(dialogMsg, stationName),
                R.string.dialog_string_delete_confirm,
                R.string.dialog_string_delete_cancel,
                dialogCallback);
    }

    @Override
    public void showDeleteSuccess(String stationName) {
        Toast.makeText(getContext(), String.format(successDeletionMsg, stationName), Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void showDeleteFailed() {
        Toast.makeText(getContext(), failDeletionMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void notifyDeleteObserver(int positionDeleted) {
        mOnDeleteStationListener.onDeleteFavouriteStation(positionDeleted);
    }

    @Override
    public void setUpShare(String stationName) {
        shareBtn.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                mStationDetailPresenter.getStationInformation();
            }
        });
        shareBtn.setContentDescription(String.format(shareStationContentDescription, stationName));
    }

    @Override
    public void setUpDirection(String stationNAme) {
        directionBtn.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                mStationDetailPresenter.getDirection();
            }
        });
        directionBtn.setContentDescription(String.format(stationDirectionContentDescription,stationNAme));
    }

    @Override
    public void setUpDelete(String stationName) {
        delFavBtn.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                mStationDetailPresenter.deleteStation();
            }
        });

        delFavBtn.setContentDescription(String.format(deleteFavouriteContentDescription, stationName));
    }

    @Override
    public void setUpFavourite() {
        favBtn.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                mStationDetailPresenter.toggleFavouriteStatus();
            }
        });

        //content description will be set in showFavouriteMarked/unmarked
    }
}
