package com.mulyadi.tofuelnsw.ui.result;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.util.AttributeSet;

import com.mulyadi.tofuelnsw.R;

/**
 * Created by Welly Mulyadi on 13/12/2017.
 */

public class SortByActionView extends SortActionView{

    private int resourceId;

    public SortByActionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        String sortBy = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(context.getString(R.string.sort_by_key), context.getString(R.string.distance_entry_value));

        if(sortBy.equals(context.getString(R.string.distance_entry_value))){
            resourceId = R.drawable.ic_sort_by_distance;
            setContentDescription(context.getString(R.string.sort_by_distance_content_description));
        }else{
            resourceId = R.drawable.ic_sort_by_price;
            setContentDescription(context.getString(R.string.sort_by_price_content_description));
        }
        setImageResource(resourceId);
        setClickable(true);
        setFocusable(true);
    }

    public void setResourceId(){
        String value;

        if(resourceId == R.drawable.ic_sort_by_price){
        resourceId = R.drawable.ic_sort_by_distance;
        setContentDescription(getContext().getString(R.string.sort_by_distance_content_description));
        value = getContext().getString(R.string.distance_entry_value);
        }else{
        resourceId = R.drawable.ic_sort_by_price;
            setContentDescription(getContext().getString(R.string.sort_by_price_content_description));
            value = getContext().getString(R.string.price_entry_value);
        }

        setImageResource(resourceId);
        PreferenceManager.getDefaultSharedPreferences(getContext())
                .edit()
                .putString(getContext().getString(R.string.sort_by_key), value)
                .apply();
    }
}
