package com.mulyadi.tofuelnsw.ui.result;

import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.main.AuthenticateUserCallback;
import com.mulyadi.tofuelnsw.ui.main.DataFetchedEventListener;
import com.mulyadi.tofuelnsw.ui.main.FuelApiResponse.FuelByLocationResponse;

import static com.mulyadi.tofuelnsw.Rest.FuelApiClient.NETWORK_ERROR_CODE;

/**
 * Created by Welly Mulyadi on 2/03/2018.
 */

public class ResultsPresenter implements FetchResultsContract.FetchPresenter, DataFetchedEventListener {

    private final String LOG_TAG = getClass().getSimpleName();
    private FetchResultsContract.ResultsView mResultsView;
    private ResultRepository mResultsRepository;
    private ResultAuthUserCallback mResultAuthUserCallback;
    private boolean isResultsShown;


    public ResultsPresenter(ResultRepository resultsRepository, FetchResultsContract.ResultsView resultsView) {
        mResultsView = resultsView;
        mResultsRepository = resultsRepository;
        mResultAuthUserCallback = new ResultAuthUserCallback();
        isResultsShown = false;
    }

    @Override
    public void start() {
        mResultsView.hideError();
        mResultsView.showContentLoading();
        if(mResultsRepository.isCacheResultUpToDate()){
            mResultsView.hideContentLoading();
            if (!isResultsShown)
                presentResults(mResultsRepository.getCachedResult(true),
                    !mResultsRepository.isAppOnline());
            return;
        }

        mResultsRepository.authenticateUser(getClass(), mResultAuthUserCallback);

    }

    @Override
    public void stop() {
        mResultsRepository.removeAuth(getClass());
    }

    @Override
    public void refresh() {
        mResultsView.hideError();
        mResultsView.showContentLoading();
        mResultsRepository.removeAuth(getClass());
        mResultsRepository.authenticateUser(getClass(), mResultAuthUserCallback);
    }

    @Override
    public void onSuccessDataFetched(Object result) {
        mResultsView.hideContentLoading();
        //if null then something wrong with the FuelAPI server
        //before we show the error message, let's see whether we have cache result for this query
        if(result == null){
            mResultsRepository.fetchMostRecent(new MostRecentMostRecentFetchCallback(true));
        }else{
           presentResults((FuelByLocationResponse) result, false);
        }

    }

    private void presentResults(FuelByLocationResponse result, boolean isOffline){
        mResultsView.showUserLocation(mResultsRepository.getAddress());
        if(isOffline){
            mResultsView.showOfflineDataUsage();
        }else {
            mResultsView.hideOfflineDataUsage();
        }
        mResultsView.addResults(result);
        isResultsShown = true;
    }

    @Override
    public void onFailDataFetched(int errCode) {
        mResultsView.hideContentLoading();
        //networkFault means the user has no internet connectivity
        if(errCode == NETWORK_ERROR_CODE){
            mResultsRepository.fetchMostRecent(new MostRecentMostRecentFetchCallback(false));
        }else{ //if not networkFault, then there's no results for user's location
            determineAddressFetchFailure(mResultsRepository.getAddress());
        }
    }

    @Override
    public void determineAddressFetchFailure(String address) {
        if(address == null){
            return;
        }

        //NSW addresses from google results may not contain "Australia" in it, so "Australia" key does not need to be checked.
        if(!mResultsRepository.validateAddress(address)){
            mResultsView.showInvalidAddressError();
        }else{
            //No stations found at the given nsw suitable address
            mResultsView.showEmptyResultsError();
        }
    }

    @Override
    public boolean isUserLocationFetched() {
        return mResultsRepository.getUserLocation() != null;
    }

    private class ResultAuthUserCallback implements AuthenticateUserCallback{
        @Override
        public void authenticationSuccess() {
            mResultsRepository.fetchResults(ResultsPresenter.this);
        }

        @Override
        public void authenticationFailure() {
            mResultsView.showAuthenticationError();
        }
    }

    private class MostRecentMostRecentFetchCallback implements MostRecentFetchCallback {

        boolean isComingFromServerError;

        private MostRecentMostRecentFetchCallback(boolean isComingFromServerError){
            this.isComingFromServerError = isComingFromServerError;
        }

        @Override
        public void onMostRecentFetched(FuelByLocationResponse mostRecentResult) {
            if(mostRecentResult == null){
                if(isComingFromServerError) {
                    mResultsView.showServerError();
                }else {
                    mResultsView.showConnectionError();
                }
            }else{
                presentResults(mostRecentResult, true);
            }
        }
    }
}
