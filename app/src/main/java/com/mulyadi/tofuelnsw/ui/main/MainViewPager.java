package com.mulyadi.tofuelnsw.ui.main;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Welly Mulyadi on 24/11/2017.
 */

public class MainViewPager extends ViewPager{

    public MainViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        //For disabling swipe (design objective)
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        //For disabling swipe (design objective)
        return false;
    }

}
