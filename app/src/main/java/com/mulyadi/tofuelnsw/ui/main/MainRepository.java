package com.mulyadi.tofuelnsw.ui.main;

import com.mulyadi.tofuelnsw.ui.base.BaseRepository;

public interface MainRepository extends BaseRepository{

    void linkUserToApp(Object acct, AuthenticateUserCallback authenticateUserCallback);
    boolean hadAskUserToSignIn();
    void setAskUserToSignIn();
}
