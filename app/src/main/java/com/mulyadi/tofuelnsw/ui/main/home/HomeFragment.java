package com.mulyadi.tofuelnsw.ui.main.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLngBounds;
import com.mulyadi.tofuelnsw.Data.UserLocation;
import com.mulyadi.tofuelnsw.Data.UserSetting;
import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.Util.DevicePermissionHandler;
import com.mulyadi.tofuelnsw.Util.DialogCallback;
import com.mulyadi.tofuelnsw.Util.MyLocationFinder;
import com.mulyadi.tofuelnsw.Util.Repository;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.main.MainActivity;
import com.mulyadi.tofuelnsw.ui.result.ResultActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Welly Mulyadi on 14/03/2018.
 */
public class HomeFragment extends Fragment implements HomeContract.View {


    final String LOG_TAG = HomeFragment.class.getSimpleName();
    final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 0;
    final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    @BindString(R.string.home_list_size)
    String homeListSizeKey;
    int homeListSize = -1;
    @BindString(R.string.home_list_position)
    String listPositionKey;
    int listPosition = -1;
    @BindString(R.string.selected_home_list_item_positions)
    String selectedHomeListItemPosKey;
    ArrayList<Integer> selectedPositions = null;

    @BindView(R.id.historyLoadingBar)
    ProgressBar historyLoadingBar;
    @BindView(R.id.recentSearchRecView)
    RecyclerView recentSearchRecView;
    @BindView(R.id.errorHistoryTextView)
    TextView errorHistoryTextView;
    @BindView(R.id.actionSearch)
    ImageButton actionSearchBtn;
    @BindView(R.id.actionMyLocation)
    ImageButton actionMyLocBtn;
    @BindView(R.id.homeToolbar)
    Toolbar toolbar;

    LinearLayoutManager mLayoutManager;

    LocationSearchedListAdapter mAdapter;
    UserLocation mUserLocation = null;
    HomePresenter mHomePresenter;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        if (getActivity() != null)
            ((MainActivity) getActivity()).setSupportActionBar(toolbar);
        getActivity().setTitle(null);

        mAdapter = new LocationSearchedListAdapter(getActivity(), new ArrayList<UserSetting>());
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recentSearchRecView.setAdapter(mAdapter);
        recentSearchRecView.setLayoutManager(mLayoutManager);

        mHomePresenter = new HomePresenter(this,
                new DevicePermissionHandler(this),
                Repository.getHomeInstance(getContext()),
                new MyLocationFinder(getActivity()));

        actionMyLocBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mHomePresenter.findMyLocation(false);
            }
        });


        actionSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mHomePresenter.searchLocation();
            }
        });


        if (savedInstanceState != null) {
            listPosition = savedInstanceState.getInt(listPositionKey, -1);
            homeListSize = savedInstanceState.getInt(homeListSizeKey, -1);
            selectedPositions = (savedInstanceState.containsKey(selectedHomeListItemPosKey)) ?
                    savedInstanceState.getIntegerArrayList(selectedHomeListItemPosKey) : null;
        }

        return view;
    }

    private void setLocation(Place place) {
        mUserLocation = new UserLocation(place);
        Util.launchIntent(getActivity(), ResultActivity.class, getString(R.string.userLocation), mUserLocation);
    }

    @Override
    public void onStart() {
        super.onStart();
        mHomePresenter.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mHomePresenter != null) {
            mHomePresenter.stop();
            mHomePresenter = null;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                setLocation(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                Util.logDebug(LOG_TAG, status.getStatusMessage());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            mHomePresenter.findMyLocation(true);
        }

    }

    @Override
    public void showLoading() {
        historyLoadingBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        historyLoadingBar.setVisibility(View.GONE);
    }

    @Override
    public void showHistory(List<UserSetting> history) {
        mAdapter.addHistory(history);
    }

    @Override
    public void showResultFindings(UserLocation userLocation) {
        Util.launchIntent(getActivity(), ResultActivity.class, getString(R.string.userLocation), userLocation);
    }

    @Override
    public void showHistoryRetrievalError() {
        errorHistoryTextView.setText(R.string.history_retrieval_issue);
    }

    @Override
    public void showNoHistoryFoundError() {
        errorHistoryTextView.setText(R.string.no_history);
    }

    @Override
    public void hideError() {
        errorHistoryTextView.setText(R.string.empty_error);
    }

    @Override
    public void showLocationSearch(LatLngBounds searchBound) {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .setBoundsBias(searchBound)
                            .build(getActivity());
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesNotAvailableException e) {
            Util.logDebug(LOG_TAG, e.toString());
        } catch (GooglePlayServicesRepairableException e) {
            Util.logDebug(LOG_TAG, e.toString());
        }

    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (mAdapter.getActionMode() != null) {
            outState.putIntegerArrayList(selectedHomeListItemPosKey, mAdapter.getSelectedPositions());
        }

        outState.putInt(listPositionKey, mLayoutManager.findFirstVisibleItemPosition());
        outState.putInt(homeListSizeKey, mAdapter.getItemCount());
        super.onSaveInstanceState(outState);
    }


    class LocationSearchedListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        List<UserSetting> locationSearchedList;

        final String LOG_TAG = LocationSearchedListAdapter.class.getSimpleName();

        private MainActivity activity;
        private ActionMode actionMode = null;
        private ArrayList<Integer> selectedPositions;
        private RecyclerView mRecyclerView;
        private boolean selectedAll = false;


        public LocationSearchedListAdapter(Context context, List<UserSetting> locationSearchedList) {
            this.locationSearchedList = locationSearchedList;
            this.activity = (context instanceof MainActivity) ? (MainActivity) context : null;
            selectedPositions = new ArrayList<>();
        }

        public ActionMode getActionMode() {
            return actionMode;
        }

        public ArrayList<Integer> getSelectedPositions() {
            return selectedPositions;
        }

        public void startActionMode() {
            this.actionMode = activity.startSupportActionMode(actionModeCallback);
        }


        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);

            mRecyclerView = recyclerView;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new SearchLocationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_searched_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            final SearchLocationViewHolder searchLocationViewHolder = (SearchLocationViewHolder) holder;

            if (selectedPositions.contains(position))
                searchLocationViewHolder.recentListItemView.setSelected(true);
            else searchLocationViewHolder.recentListItemView.setSelected(false);

            String address = locationSearchedList.get(getItemCount() - position - 1)
                    .getUserLocation()
                    .getAddress();

            String timeStamp = locationSearchedList.get(getItemCount() - position - 1)
                    .getTimeFetched();

            searchLocationViewHolder.getLocationAddressTextView().setText(Util.trimAddress(address));
            searchLocationViewHolder.getLocationSuburbTextview().setText(Util.getSuburb(address));

            long timeInMillis = Util.getTimeInMillis(timeStamp);
            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(timeInMillis);

            CharSequence timeStampStr = Util.getCorrectPrefixDate(activity, date);
            searchLocationViewHolder.getLocationTimeStampTextView().setText(timeStampStr);

            searchLocationViewHolder.recentListItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (actionMode == null) {
                        Util.launchIntent(activity, ResultActivity.class, activity.getString(R.string.userLocation), locationSearchedList
                                .get(getItemCount() - searchLocationViewHolder.getAdapterPosition() - 1)
                                .getUserLocation());
                    } else {
                        if (searchLocationViewHolder.recentListItemView.isSelected())
                            removeSelectedItem(searchLocationViewHolder.getAdapterPosition());
                        else addItemsToSelected(searchLocationViewHolder.getAdapterPosition());
                    }
                }
            });

            searchLocationViewHolder.recentListItemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (actionMode == null) {
                        clearSelectedItems();
                        selectedAll = false;
                        selectedPositions.clear();
                        startActionMode();
                        addItemsToSelected(searchLocationViewHolder.getAdapterPosition());
                    }else {
                        if (searchLocationViewHolder.recentListItemView.isSelected())
                            removeSelectedItem(searchLocationViewHolder.getAdapterPosition());
                        else addItemsToSelected(searchLocationViewHolder.getAdapterPosition());
                    }
                    return true;
                }
            });

        }

        private void addHistory(List<UserSetting> history) {
            locationSearchedList = history;
            notifyDataSetChanged();

        }



        @Override
        public int getItemCount() {
            return locationSearchedList.size();
        }

        private void clearSelectedItems() {
            for (int current : selectedPositions) {
                SearchLocationViewHolder searchLocationViewHolder = (SearchLocationViewHolder) mRecyclerView.findViewHolderForAdapterPosition(current);
                //this method is also triggered at onDestroyActionMode, which is called by actionMode.finish() when we remove an item
                //Because of this, the recycler view may not have the items refreshed in time
                if (searchLocationViewHolder != null)
                    searchLocationViewHolder.recentListItemView.setSelected(false);
            }
        }

        private void addItemsToSelected(int position) {

            if (selectedPositions.size() != 0) {
                int ix = 0;
                int previousSelected = -1;
                boolean prevAddSuccess = false;

                while (ix < selectedPositions.size()) {
                    int currentSelected = selectedPositions.get(ix);
                    if (position < currentSelected && position > previousSelected) {
                        selectedPositions.add(ix, position);
                        prevAddSuccess = true;
                        break;
                    }
                    previousSelected = currentSelected;
                    ix += 1;
                }
                if (!prevAddSuccess) selectedPositions.add(position);
            } else {
                selectedPositions.add(position);
            }

            if (selectedPositions.size() == getItemCount()) selectedAll = true;
            notifyItemChanged(position);
        }

        private void removeSelectedItem(int position) {
            for (int ix = 0; ix < selectedPositions.size(); ix++) {
                if (selectedPositions.get(ix) == position) {
                    selectedPositions.remove(ix);
                    if (selectedAll) selectedAll = false;
                    break;
                }
            }
            if (selectedPositions.size() == 0) {
                actionMode.finish();
                actionMode = null;
            }
            notifyItemChanged(position);
        }

        private void selectAllItems() {
            selectedPositions.clear();
            for (int ix = 0; ix < getItemCount(); ix++) {
                selectedPositions.add(ix);
            }
            selectedAll = true;
            notifyDataSetChanged();
        }


        public class SearchLocationViewHolder extends RecyclerView.ViewHolder {


            @BindView(R.id.locationAddressTextView)
            TextView locationAddressTextView;
            @BindView(R.id.locationSuburbTextView)
            TextView locationSuburbTextview;
            @BindView(R.id.locationTimeStampTextView)
            TextView locationTimeStampTextView;
            @BindView(R.id.recentListItemView)
            View recentListItemView;


            private TextView getLocationSuburbTextview() {
                return locationSuburbTextview;
            }

            public TextView getLocationTimeStampTextView() {
                return locationTimeStampTextView;
            }


            private SearchLocationViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            public TextView getLocationAddressTextView() {
                return locationAddressTextView;
            }

        }

        ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                activity.getMenuInflater().inflate(R.menu.edit_menu, menu);
                activity.getMyNavigation().setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (item.getItemId() == R.id.selectOrUnselectAll) {
                    if (selectedAll) {
                        clearSelectedItems();
                        selectedAll = false;
                        selectedPositions.clear();
                    } else selectAllItems();
                } else {
                    Util.showDialog(activity,
                            activity.getString(R.string.dialog_string_delete_search),
                            R.string.dialog_string_delete_confirm,
                            R.string.dialog_string_delete_cancel,
                            new DialogCallback() {
                                @Override
                                public void confirmedClicked() {
                                    mHomePresenter.deleteHistory(selectedPositions, new OnHistoryDeleteListener() {
                                        @Override
                                        public void onHistoryDeletedSuccess(boolean isEmpty) {
                                            if(actionMode!= null)
                                                actionMode.finish();
                                            notifyDataSetChanged();
                                            if(isEmpty)
                                                showNoHistoryFoundError();

                                        }
                                    });
                                }
                            });
                }
                return true;
            }


            @Override
            public void onDestroyActionMode(ActionMode mode) {
                activity.getMyNavigation().setVisibility(View.VISIBLE);
                actionMode = null;
                clearSelectedItems();
                selectedAll = false;
                selectedPositions.clear();
            }
        };

    }

    public  interface OnHistoryDeleteListener{
        void onHistoryDeletedSuccess(boolean isEmpty);
    }
}
