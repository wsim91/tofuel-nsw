package com.mulyadi.tofuelnsw.ui.base;

import com.mulyadi.tofuelnsw.ui.main.AuthenticateUserCallback;

/**
 * Created by Welly Mulyadi on 14/03/2018.
 */

public interface BaseRepository {
    boolean isUserAuthenticated();
    void authenticateUser(Class source, AuthenticateUserCallback authenticateUserCallback);
    void removeAuth(Class source);
    boolean isAppOnline();
}
