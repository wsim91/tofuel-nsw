package com.mulyadi.tofuelnsw.ui.detail;

/**
 * Created by Welly Mulyadi on 13/03/2018.
 */

public interface StationDeleteCallback {
    void deleteComplete(String stationName, int positionToBeDeleted);
    void deleteFailed();
}
