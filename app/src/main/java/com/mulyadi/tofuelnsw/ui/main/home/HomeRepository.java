package com.mulyadi.tofuelnsw.ui.main.home;

import com.google.android.gms.maps.model.LatLngBounds;
import com.mulyadi.tofuelnsw.Data.UserSetting;
import com.mulyadi.tofuelnsw.ui.base.BaseRepository;

import java.util.List;

/**
 * Created by Welly Mulyadi on 20/03/2018.
 */

public interface HomeRepository extends BaseRepository{
    void fetchHistory(FetchHistoryCallback fetchHistoryCallback);
    void updateHistory();
    boolean isLocalHistoryDataUpToDate();
    void setHistoryStatus(boolean isUpToDate);
    List<UserSetting> getLocalHistory();
    LatLngBounds getSearchBound();
}
