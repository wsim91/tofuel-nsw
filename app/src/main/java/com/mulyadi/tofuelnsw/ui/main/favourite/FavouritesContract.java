package com.mulyadi.tofuelnsw.ui.main.favourite;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.ui.base.BasePresenter;

import java.util.List;

/**
 * Created by Welly Mulyadi on 14/03/2018.
 */

public interface FavouritesContract {
    interface View{
        void showFavourites(List<Station> favourites);
        void showNoFavouritesFoundError();
        void showDetailDialog(Station station);
    }

    interface Presenter extends BasePresenter{
        int findStationPositionInList(int stationCode);
        void openFavouriteDetail(int stationPos);
        void resetIsShown();
    }
}
