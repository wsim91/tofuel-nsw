package com.mulyadi.tofuelnsw.ui.base;

/**
 * Created by Welly Mulyadi on 14/03/2018.
 */
public interface BaseView {
    void showLoading();
    void hideLoading();
}
