package com.mulyadi.tofuelnsw.ui;

/**
 * Created by Welly Mulyadi on 24/08/2017.
 */

public interface AccessTokenEventListener {
    void onSuccess(String accessToken);
    void onFailed();

}
