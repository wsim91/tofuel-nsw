package com.mulyadi.tofuelnsw.ui.widget;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.ui.base.BasePresenter;
import com.mulyadi.tofuelnsw.ui.base.BaseView;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;

import java.util.List;

public interface WidgetContract {

    interface WidgetConfigurationContract{
        interface View extends BaseView{
            void attachFavourites(List<Station> favourites);
            void clearFavourites();
            void completeConfiguration();
            void dismissConfiguration();
            void showEmptyFavouritesInformation();
            void hideEmptyFavouritesInformation();

        }

        interface Presenter extends BasePresenter{
            void saveConfiguration(int id, Station selectedStation);
        }
    }

    interface WidgetDisplayContract{
        interface View{
            void showLoading(int appWidgetId);
            void showError(int appWidgetId, int errCode, Station station);
            void showStationDetail(int appWidgetId, AllPetrolPriceByStationResponse detail, Station station);
        }

        interface Presenter extends BasePresenter{
        }
    }

}
