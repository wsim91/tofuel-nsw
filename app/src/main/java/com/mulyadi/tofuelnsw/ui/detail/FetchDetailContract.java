package com.mulyadi.tofuelnsw.ui.detail;

import android.net.Uri;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.Util.DialogCallback;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.ui.base.BasePresenter;

/**
 * Created by Welly Mulyadi on 5/03/2018.
 */

public interface FetchDetailContract {
    interface View {
        void addBasicStationInfo(Station station);
        void addDetail(AllPetrolPriceByStationResponse allPetrolPriceByStationResponse);
        void setFadeAnimationToFavMarking(boolean fadeIn);
        void disableMarkingFavourite();
        void enableMarkingFavourite();
        void showContentLoading();
        void hideContentLoading();
        void showFavouriteLoading();
        void hideFavouriteLoading();
        void showConnectivityError();
        void hideConnectivityError();
        void showFavouriteMode();
        void hideFavouriteMode();
        void showResultMode();
        void hideResultMode();
        void showServerError();
        void hideServerError();
        void showDeleteConfirmation(String stationName, DialogCallback dialogCallback);
        void showDeleteSuccess(String stationName);
        void showDeleteFailed();
        void showAuthenticationError();
        void hideAuthenticationError();
        void showFavouriteMarked(String stationName);
        void showFavouriteUnmarked(String stationName);
        void shareStationInformation(String sharedInformationStr);
        void showDirection(Uri geoStationURI);
        void notifyDeleteObserver(int positionDeleted);
        void setUpShare(String stationName);
        void setUpDirection(String stationNAme);
        void setUpDelete(String stationName);
        void setUpFavourite();

    }

    interface Presenter extends BasePresenter{
        void getStationInformation();
        void getDirection();
        void toggleFavouriteStatus();
        void deleteStation();
        void setUpShareFunctionality();
        void setUpDirectionFunctionality();
        void setUpFavOrDeleteFunctionality(boolean isFavMode);
    }
}
