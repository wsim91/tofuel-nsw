package com.mulyadi.tofuelnsw.ui.main.favourite;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.ui.main.AuthenticateUserCallback;

import java.util.List;


/**
 * Created by Welly Mulyadi on 14/03/2018.
 */

public class FavouritePresenter implements FavouritesContract.Presenter{


    private static final int UNKNOWN = -1;
    private static final int FETCH_MODE = 0;
    private static final int CLICK_MODE = 1;

    private FavouritesContract.View mFavView;
    private FavouritesRepository mFavRepository;
    private FavouriteAuthUserCallback mFavAuthUserCallback;
    private boolean isFavouritesShown;

    public FavouritePresenter(FavouritesContract.View favView, FavouritesRepository favouritesRepository) {
        mFavView = favView;
        mFavRepository = favouritesRepository;
        mFavAuthUserCallback = new FavouriteAuthUserCallback();
        isFavouritesShown = false;
    }

    @Override
    public void start() {

        if(mFavRepository.isFavDataLoaded()) {
            if (!isFavouritesShown){
                isFavouritesShown = true;
                mFavView.showFavourites(mFavRepository.getLoadedFavourites());
            }
            return;
        }

        loadFavourites();

    }

    @Override
    public void stop() {
        mFavRepository.removeAuth(getClass());

    }

    @Override
    public void refresh() {
        loadFavourites();
    }

    private void loadFavourites() {
        mFavRepository.setFavouritesUpToDate(false);
        mFavAuthUserCallback.setFetchMode();
        mFavRepository.authenticateUser(getClass(), mFavAuthUserCallback);

    }

    @Override
    public int findStationPositionInList(int stationCode) {
        List<Station> favourites = mFavRepository.getLoadedFavourites();
        if(stationCode == -1 || favourites == null || favourites.size() <= 0) {
            return -1;
        }
        int stationPosition = -1;

        for(int position = 0; position < favourites.size(); position++){
            if(favourites.get(position).getCode() == stationCode){
                stationPosition = position;
                break;
            }
        }

        return stationPosition;
    }

    @Override
    public void openFavouriteDetail(final int stationPos) {
        mFavAuthUserCallback.setClickMode(stationPos);
        mFavRepository.authenticateUser(getClass(), mFavAuthUserCallback);
    }

    @Override
    public void resetIsShown() {
        isFavouritesShown = false;
    }

    private class FavouriteAuthUserCallback implements AuthenticateUserCallback{
        int mode;
        int mPosClicked;

        private FavouriteAuthUserCallback(){
            mode = UNKNOWN;
            mPosClicked = -1;
        }

        public void setFetchMode(){
            mode = FETCH_MODE;
        }

        public  void setClickMode(int positionClicked){
            mode = CLICK_MODE;
            mPosClicked = positionClicked;
        }


        @Override
        public void authenticationSuccess() {
            switch (mode){
                case FETCH_MODE:
                    mFavRepository.fetchFavourites(new FetchFavouritesCallback() {
                        @Override
                        public void onFetchComplete(List<Station> favourites) {
                            if(favourites == null || favourites.size() == 0){
                                mFavView.showNoFavouritesFoundError();
                            }else{
                                mFavView.showFavourites(favourites);
                                isFavouritesShown = true;
                            }
                        }

                    });
                    break;
                case CLICK_MODE:
                    mFavView.showDetailDialog(mFavRepository.getLoadedFavourites().get(mPosClicked));
                    break;
            }

        }

        @Override
        public void authenticationFailure() {
            stop();
        }
    }

}
