package com.mulyadi.tofuelnsw.ui.result;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

/**
 * Created by Welly Mulyadi on 2/03/2018.
 */

public abstract class SortActionView extends AppCompatImageButton {

    public SortActionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public abstract void setResourceId();
}
