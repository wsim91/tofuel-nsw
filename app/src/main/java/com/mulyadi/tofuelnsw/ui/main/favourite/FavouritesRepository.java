package com.mulyadi.tofuelnsw.ui.main.favourite;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.ui.base.BaseRepository;

import java.util.List;

/**
 * Created by Welly Mulyadi on 14/03/2018.
 */

public interface FavouritesRepository extends BaseRepository{

    boolean isFavDataLoaded();
    void fetchFavourites(FetchFavouritesCallback fetchFavouritesCallback);
    void setFavouritesUpToDate(boolean upToDate);
    List<Station> getLoadedFavourites();

}
