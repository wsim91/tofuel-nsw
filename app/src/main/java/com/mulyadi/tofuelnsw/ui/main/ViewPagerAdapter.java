package com.mulyadi.tofuelnsw.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.main.favourite.FavouritesFragment;
import com.mulyadi.tofuelnsw.ui.main.home.HomeFragment;

/**
 * Created by Welly Mulyadi on 22/02/2018.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private static final String LOG_TAG = ViewPagerAdapter.class.getSimpleName();
    public static final int HOME_FRAGMENT_POSITION = 0;
    public static final int FAVOURITE_FRAGMENT_POSITION = 1;
    public static final int MAX_FRAGMENTS = 2;

    private Context mContext;
    private Bundle mFavouriteBundle;
    private Fragment mCurrentFragment;
    private int stationCode;

    public ViewPagerAdapter(FragmentManager fm, Context context, Bundle favouriteBundle) {
        super(fm);
        mContext = context;
        mFavouriteBundle = favouriteBundle;
        stationCode = -1;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch(position){
            case FAVOURITE_FRAGMENT_POSITION:
                fragment = new FavouritesFragment();
                if(mFavouriteBundle != null){
                    fragment.setArguments(mFavouriteBundle);
                }
                break;
            default:
                fragment = new HomeFragment();

        }
        return fragment;
    }
    public Fragment getCurrentFragment(){
        return mCurrentFragment;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if(getCurrentFragment() != object){
            mCurrentFragment = (Fragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public int getCount() {
        return MAX_FRAGMENTS;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {

        if(object instanceof FavouritesFragment && stationCode != -1){
            ((FavouritesFragment)object).passOnStationCode(stationCode);
            stationCode =-1;
        }

        return super.getItemPosition(object);
    }

    public void setUpdateStationClick(int stationCode){
        this.stationCode = stationCode;
    }
}
