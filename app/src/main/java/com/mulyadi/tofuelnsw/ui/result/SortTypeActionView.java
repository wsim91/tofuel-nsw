package com.mulyadi.tofuelnsw.ui.result;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

import com.mulyadi.tofuelnsw.R;

/**
 * Created by Welly Mulyadi on 13/12/2017.
 */

public class SortTypeActionView extends SortActionView{

    private int resourceId;

    public SortTypeActionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        boolean sortAscending = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getBoolean(context.getString(R.string.sort_ascending_key), context.getResources().getBoolean(R.bool.ascending_default_entry));

        if(sortAscending){
            resourceId = R.drawable.ic_sort_asc;
            setContentDescription(context.getString(R.string.sort_ascend_content_description));
        }else{
            resourceId = R.drawable.ic_sort_desc;
            setContentDescription(context.getString(R.string.sort_descend_content_description));
        }
        setImageResource(resourceId);
        setClickable(true);
        setFocusable(true);
    }

    public void setResourceId(){
        boolean value;

        if(resourceId == R.drawable.ic_sort_asc){
            resourceId = R.drawable.ic_sort_desc;
            setContentDescription(getContext().getString(R.string.sort_descend_content_description));
            value = false;
        }else{
            resourceId = R.drawable.ic_sort_asc;
            setContentDescription(getContext().getString(R.string.sort_ascend_content_description));
            value = true;
        }

        setImageResource(resourceId);
        PreferenceManager.getDefaultSharedPreferences(getContext())
                .edit()
                .putBoolean(getContext().getString(R.string.sort_ascending_key), value)
                .apply();
    }


}
