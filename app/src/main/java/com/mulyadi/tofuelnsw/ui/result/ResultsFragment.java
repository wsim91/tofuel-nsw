package com.mulyadi.tofuelnsw.ui.result;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mulyadi.tofuelnsw.Data.StationPrice;
import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.Data.UserLocation;
import com.mulyadi.tofuelnsw.Util.DevicePermissionHandler;
import com.mulyadi.tofuelnsw.Util.FetchLocationCallback;
import com.mulyadi.tofuelnsw.Util.LocationFinder;
import com.mulyadi.tofuelnsw.Util.MyLocationFinder;
import com.mulyadi.tofuelnsw.Util.UserPreference;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.detail.DialogStationFragment;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.ui.main.FuelApiResponse.FuelByLocationResponse;
import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.ui.main.FuelRecViewAdapter;
import com.mulyadi.tofuelnsw.ui.main.MoreActionView;
import com.mulyadi.tofuelnsw.ui.main.OnStationClickedCallback;
import com.mulyadi.tofuelnsw.ui.main.OverflowActionOnClickListener;
import com.mulyadi.tofuelnsw.Util.Repository;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultsFragment extends Fragment implements FetchResultsContract.ResultsView, OnStationClickedCallback {

    final String LOG_TAG = ResultsFragment.class.getSimpleName();
    public static final int LOCATION_SERVICES_UNAVAILABLE = -1;
    public static final int NO_SECURITY_PERMISSION_CODE = 0;
    public static final int STATUS_OK = 1;

    ResultActivity mainActivity;

    FuelRecViewAdapter mFuelRecViewAdapter;
    @BindView(R.id.resultToolbar)
    Toolbar toolbar;
    @BindView(R.id.resultLoadingBar)
    ProgressBar resultLoadingBar;
    @BindView(R.id.recView)
    RecyclerView mRecView;
    @BindView(R.id.myCoordinatorLayout)
    CoordinatorLayout myCoordinatorLayout;
    @BindView(R.id.sortByAction)
    SortByActionView sortByActionView;
    @BindView(R.id.sortTypeAction)
    SortTypeActionView sortTypeActionView;
    @BindView(R.id.moreAction)
    MoreActionView moreActionView;
    @BindView(R.id.toFuelFragmentErrorTextView)
    TextView errorTextView;
    @BindView(R.id.userLocationTextView)
    TextView userLocationTextView;
    Snackbar mOfflineDataUsedSnackBar;


    @BindString(R.string.title_results)
    String fragmentTitleStr;
    @BindString(R.string.expanded_position)
    String expandedPosKey;
    @BindString(R.string.first_visible_position)
    String firstVisiblePosKey;
    @BindString(R.string.station_fuel_details)
    String stationFuelDetailsKey;
    @BindString(R.string.detail_source)
    String detailSourceKey;
    @BindString(R.string.selectedStation)
    String selectedStationKey;
    @BindString(R.string.result_fragment_location_fetched)
    String locationFetchedKey;

    LinearLayoutManager mLinearLayoutManager;

    int lastClicked = -1;
    int firstVisiblePosition = -1;

    ResultsPresenter mResultsPresenter;
    LocationFinder mLocationFinder;

    FetchLocationCallback mFetchLocationCallback = new FetchLocationCallback() {
        @Override
        public void onFetched(UserLocation userLocation) {
            init(userLocation);
            mResultsPresenter.start();
        }

        @Override
        public void onFailed(int errCode) {
            hideContentLoading();
            if(errCode == LOCATION_SERVICES_UNAVAILABLE){
                determineSpecificLocationServiceErrorAndShow();
            }else{
                showLocationPermissionError();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (ResultActivity)getActivity();


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.list_station_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return true;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View toFuelNSWView = inflater.inflate(R.layout.fragment_results, container, false);

        ButterKnife.bind(this, toFuelNSWView);
        //clearAllErrorViews();


        if(savedInstanceState != null){
            if(savedInstanceState.containsKey(expandedPosKey)) {
                lastClicked = savedInstanceState.getInt(expandedPosKey, -1);
            }
            if(savedInstanceState.containsKey(firstVisiblePosKey)){
                firstVisiblePosition = savedInstanceState.getInt(firstVisiblePosKey, -1);
            }
            //if savedInstanceState exist, that means mUserLocation also exist.
            //Though we can grab it from getIntent(), this isn't available if we were fetching via requestLocationUpdates()
            //So in this case, we can simply re-use the data in repository
            userLocationDeterminer(savedInstanceState.getBoolean(locationFetchedKey));
        }else{
            userLocationDeterminer(false);
        }

        return toFuelNSWView;
    }

    /**
     * The purpose of this method is to determine whether the location has been fetched yet or not.
     * In cases where the user hasn't turned on Location, if the screen is rotated,
     * it should try to re-fetch the location again. This method helps if screen is rotated multiple times,
     * and if one of the instance scenario occurs to fetched succesfully, we do not need to refetch it since it is in cache.
     * @param locationFetched indicator for determining whether we have fetched the location yet or not
     */
    private void userLocationDeterminer(boolean locationFetched){
        if(!locationFetched){
            UserLocation userLocation = getActivity().getIntent().getParcelableExtra(getString(R.string.userLocation));
            //if location is null from intent, it means that there was no location available in getLastLocation()
            //Due to this, we need to fetch the data using requestLocationUpdates()
            if(userLocation == null){
                mLocationFinder = new MyLocationFinder(getActivity());
                mLocationFinder.fetchMyLocation(mFetchLocationCallback);
            }else{
                init(userLocation);
            }
        }else{
            //fetch from cache
            init(null);
        }
    }

    private void init(UserLocation userLocation){
        mResultsPresenter = new ResultsPresenter(
                Repository.getResultInstance(getContext(), userLocation),
                this);
        setUpToolbar();
        setUpAdapter();
    }

    private void determineSpecificLocationServiceErrorAndShow(){
        DevicePermissionHandler dph = new DevicePermissionHandler(this);
        if(!dph.hasGPSEnabled()){
            showLocationDisabledError();
        }else if(!dph.isNetworkAvailable()){

            showConnectionError();
        }else{
            //problem with the actual location service itself
            showLocationServicesError();
        }
    }
    private void setUpToolbar(){
        sortByActionView.setOnClickListener(new SortActionOnClickListener());
        sortTypeActionView.setOnClickListener(new SortActionOnClickListener());
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        final OverflowActionOnClickListener overflowActionOnClickListener = new OverflowActionOnClickListener(moreActionView);
        moreActionView.setOnClickListener(overflowActionOnClickListener);
        overflowActionOnClickListener.getPopupMenu().setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            int menuTypeId = -1;
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(item.getItemId()== R.id.fuelTypeAction || item.getItemId()== R.id.radiusAction){
                    menuTypeId = item.getItemId();
                }else if (menuTypeId == R.id.fuelTypeAction && item.getItemId() != menuTypeId) {
                    PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putInt(getString(R.string.fueltype_key), item.getOrder()).apply();
                    item.setIcon(R.drawable.ic_check_black);
                    overflowActionOnClickListener.getPrefItemFuelType().setIcon(null);
                    overflowActionOnClickListener.setPrefItemFuelType(item);
                    mResultsPresenter.refresh();
                    menuTypeId = -1;
                }else if(menuTypeId == R.id.radiusAction && item.getItemId() != menuTypeId){
                    PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putInt(getString(R.string.radius_key), item.getOrder()).apply();
                    item.setIcon(R.drawable.ic_check_black);
                    overflowActionOnClickListener.getPrefItemRadius().setIcon(null);
                    overflowActionOnClickListener.setPrefItemRadius(item);
                    mResultsPresenter.refresh();
                    menuTypeId = -1;
                }

                return true;
            }});
    }


    public void requestLocationUpdates(int statusCode) {
        if(statusCode == STATUS_OK) {
            mLocationFinder.fetchMyLocation(mFetchLocationCallback);
        }else{
            mFetchLocationCallback.onFailed(statusCode);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        //possible that it is null if data is empty/unable to be retrieved
        if(mLinearLayoutManager != null)
            outState.putInt(firstVisiblePosKey, mLinearLayoutManager.findFirstVisibleItemPosition());
        outState.putBoolean(locationFetchedKey,mResultsPresenter.isUserLocationFetched());
        super.onSaveInstanceState(outState);
    }



    private void setUpAdapter(){

        mFuelRecViewAdapter = new FuelRecViewAdapter(getActivity(), new FuelByLocationResponse(new ArrayList<Station>(), new ArrayList<StationPrice>()), this);
        mLinearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        mRecView.setLayoutManager(mLinearLayoutManager);
        mRecView.setAdapter(mFuelRecViewAdapter);

    }

    @Override
    public void onStart() {
        super.onStart();
        if(mResultsPresenter != null) {
            mResultsPresenter.start();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mResultsPresenter != null) {
            mResultsPresenter.stop();
            mResultsPresenter = null;
        }
    }

    @Override
    public void onStationClicked(Station station) {
        DialogStationFragment detailFragment = new DialogStationFragment();
        Bundle detailBundle = new Bundle();
        detailBundle.putString(detailSourceKey,fragmentTitleStr);
        detailBundle.putParcelable(selectedStationKey, station);
        detailFragment.setArguments(detailBundle);
        detailFragment.show(getChildFragmentManager(), null);
    }


    @Override
    public void addResults(FuelByLocationResponse fuelByLocationResponse) {
        mFuelRecViewAdapter.updateData(fuelByLocationResponse);
        mLinearLayoutManager.scrollToPosition(firstVisiblePosition!= -1 ? firstVisiblePosition:0);
    }

    @Override
    public void showOfflineDataUsage() {
        mFuelRecViewAdapter.setOfflineMode(true);
        mOfflineDataUsedSnackBar = Snackbar.make(myCoordinatorLayout, R.string.offline_data_used, Snackbar.LENGTH_INDEFINITE);
        mOfflineDataUsedSnackBar.setAction(R.string.retry, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOfflineDataUsedSnackBar.dismiss();
                userLocationDeterminer(mResultsPresenter.isUserLocationFetched());
                mResultsPresenter.refresh();
            }
        });
        mOfflineDataUsedSnackBar.show();
    }

    @Override
    public void hideOfflineDataUsage() {
        mFuelRecViewAdapter.setOfflineMode(false);
        if(mOfflineDataUsedSnackBar != null && mOfflineDataUsedSnackBar.isShown()){
            mOfflineDataUsedSnackBar.dismiss();
            mFuelRecViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showUserLocation(String address) {
        userLocationTextView.setText(address);
    }
    @Override
    public void showEmptyResultsError() {
        errorTextView.setText(getString(R.string.no_data_available));
    }
    @Override
    public void showInvalidAddressError() {
        errorTextView.setText(getString(R.string.nearby_address_not_in_nsw_error));
    }
    @Override
    public void showConnectionError() {
        errorTextView.setText(R.string.no_internet_connection);
    }
    @Override
    public void showLocationServicesError() {
        errorTextView.setText(R.string.location_service_unavailable);
    }
    @Override
    public void showLocationDisabledError() {
        errorTextView.setText(R.string.gps_disabled);
    }

    @Override
    public void hideError() {
        errorTextView.setText(R.string.empty_error);
    }

    @Override
    public void showServerError() {
        errorTextView.setText(R.string.server_not_available);
    }

    @Override
    public void showContentLoading() {
        resultLoadingBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideContentLoading() {
        resultLoadingBar.setVisibility(View.GONE);
    }

    @Override
    public void showAuthenticationError() {
        errorTextView.setText(R.string.authentication_failed);
    }

    @Override
    public void showLocationPermissionError() {
        errorTextView.setText(R.string.no_gps_permission_granted);
    }

    private class SortActionOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View view) {
            if(view instanceof SortActionView){
                ((SortActionView)view).setResourceId();
                mResultsPresenter.refresh();
            }
        }
    }
}
