package com.mulyadi.tofuelnsw.ui.result;

import com.mulyadi.tofuelnsw.Data.UserLocation;
import com.mulyadi.tofuelnsw.ui.base.BaseRepository;
import com.mulyadi.tofuelnsw.ui.main.DataFetchedEventListener;
import com.mulyadi.tofuelnsw.ui.main.FuelApiResponse.FuelByLocationResponse;

/**
 * Created by Welly Mulyadi on 2/03/2018.
 */

public interface ResultRepository extends BaseRepository{

  void fetchResults(DataFetchedEventListener dataFetchedEventListener);
  void fetchMostRecent(MostRecentFetchCallback mostRecentFetchCallback);
  FuelByLocationResponse getCachedResult(boolean updateLocalHistory);
  boolean isCacheResultUpToDate();
  void setResultUpToDate(boolean isUpToDate);
  String getAddress();
  boolean validateAddress(String address);
  UserLocation getUserLocation();
}
