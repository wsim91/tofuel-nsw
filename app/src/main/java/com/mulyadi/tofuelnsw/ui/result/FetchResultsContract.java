package com.mulyadi.tofuelnsw.ui.result;

import com.mulyadi.tofuelnsw.ui.base.BasePresenter;
import com.mulyadi.tofuelnsw.ui.main.FuelApiResponse.FuelByLocationResponse;

/**
 * Created by Welly Mulyadi on 2/03/2018.
 */

public interface FetchResultsContract {
    interface ResultsView {
        void addResults(FuelByLocationResponse fuelByLocationResponse);
        void showOfflineDataUsage();
        void hideOfflineDataUsage();
        void showEmptyResultsError();
        void showUserLocation(String address);
        void showInvalidAddressError();
        void showConnectionError();
        void showLocationServicesError();
        void showLocationDisabledError();
        void showLocationPermissionError();
        void hideError();
        void showServerError();
        void showContentLoading();
        void hideContentLoading();
        void showAuthenticationError();


    }

    interface FetchPresenter extends BasePresenter{
        void determineAddressFetchFailure(String address);
        boolean isUserLocationFetched();
    }
}
