package com.mulyadi.tofuelnsw.ui.main.home;

import com.mulyadi.tofuelnsw.Data.UserSetting;

import java.util.List;

/**
 * Created by Welly Mulyadi on 20/03/2018.
 */

public interface FetchHistoryCallback {
    void onFetched(List<UserSetting> history);
    void onFailed();
}
