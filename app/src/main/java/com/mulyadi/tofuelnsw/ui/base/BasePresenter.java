package com.mulyadi.tofuelnsw.ui.base;

/**
 * Created by Welly Mulyadi on 15/03/2018.
 */

public interface BasePresenter {
    void start();
    void stop();
    void refresh();
}
