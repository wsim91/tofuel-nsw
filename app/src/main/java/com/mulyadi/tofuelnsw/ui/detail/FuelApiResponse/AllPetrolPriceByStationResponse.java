package com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse;

import com.google.gson.annotations.SerializedName;
import com.mulyadi.tofuelnsw.Data.Price;

import java.util.List;

/**
 * Created by Welly Mulyadi on 20/09/2017.
 */

public class AllPetrolPriceByStationResponse{

    @SerializedName("prices")
    private List<Price> mPrices;
    private String mTimeAccessed;
    //For initial adapter state
    public  AllPetrolPriceByStationResponse(List<Price> prices){
        mPrices = prices;
    }
    //For FRDB purposes
    public AllPetrolPriceByStationResponse(){}
    public List<Price> getPrices() {
        return mPrices;
    }

    public void setPrices(List<Price> prices) {
        mPrices = prices;
    }

    public String getTimeAccessed(){
        return mTimeAccessed;
    }

    public void setTimeAccessed(String newTimeAccessed){
        mTimeAccessed = newTimeAccessed;
    }


}