package com.mulyadi.tofuelnsw.ui.main;

/**
 * Created by Welly Mulyadi on 19/03/2018.
 */

public class MainPresenter implements MainContract.Presenter{

    private MainRepository mMainRepository;
    private MainContract.View mView;
    private AuthenticateUserCallback mAuthUserCallback = new AuthenticateUserCallback() {
        @Override
        public void authenticationSuccess() {
           authenticationSuccessState();
            mView.launchUI();
        }

        @Override
        public void authenticationFailure() {
            //mView.hideLoading();
            mMainRepository.setAskUserToSignIn();
            mView.showSignInPrompt();

        }
    };

    public MainPresenter(MainRepository mainRepository, MainContract.View view) {
        this.mMainRepository = mainRepository;
        this.mView = view;
    }

    @Override
    public void start() {
        loadUser();
    }

    private void loadUser(){
        if(!mMainRepository.isUserAuthenticated()) {
            //mView.showAppBar();
            if(!mMainRepository.hadAskUserToSignIn()) {
                showSignIn();
            }else{
                issueError();
            }
        }else{
            authenticationSuccessState();
            mView.launchUI();
        }
    }

    private void showSignIn(){
        mView.showLoading();
        mView.hideSignInFailedError();
        mMainRepository.authenticateUser(getClass(), mAuthUserCallback);
    }
    @Override
    public void stop() {
        mMainRepository.removeAuth(getClass());
    }

    @Override
    public void refresh() {
        showSignIn();
    }

    @Override
    public void issueError() {
        authenticationFailedState();
    }

    private void authenticationSuccessState(){
        mView.hideLoading();
        mView.hideSignInAction();
        mView.hideAppBar();
        mView.hideSignInFailedError();
        mView.showNavBar();
    }
    private void authenticationFailedState(){
        mView.hideLoading();
        mView.hideNavBar();
        mView.showSignInFailedError();
        mView.showAppBar();
        mView.showSignInAction();
    }

    @Override
    public void linkUserToApp(Object acct) {
        mView.showLoading();
        mMainRepository.linkUserToApp(acct, new AuthenticateUserCallback() {
            @Override
            public void authenticationSuccess() {
                authenticationSuccessState();
                mView.launchUI();
            }

            @Override
            public void authenticationFailure() {
                authenticationFailedState();
            }
        });
    }
}
