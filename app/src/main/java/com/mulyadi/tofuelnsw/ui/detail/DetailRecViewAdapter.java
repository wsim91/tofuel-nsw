package com.mulyadi.tofuelnsw.ui.detail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Welly Mulyadi on 23/02/2018.
 */

public class DetailRecViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    AllPetrolPriceByStationResponse stationFuelDetails;
    Context mContext;

    public DetailRecViewAdapter(Context context, AllPetrolPriceByStationResponse stationFuelDetails) {
        this.stationFuelDetails = stationFuelDetails;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FuelPriceDetailViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_recyclerview_petrol_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        FuelPriceDetailViewHolder fuelPriceDetailViewHolder = (FuelPriceDetailViewHolder)holder;

        fuelPriceDetailViewHolder.detailFuelTypeTextView.setText(Util.getPetrolString(stationFuelDetails.getPrices().get(position).getFuelType(), mContext));
        fuelPriceDetailViewHolder.detailFuelPriceTextView.setText(""+stationFuelDetails.getPrices().get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return stationFuelDetails.getPrices().size();
    }


    public void updateStationDetails(AllPetrolPriceByStationResponse results) {
        stationFuelDetails = results;
        notifyDataSetChanged();
    }

    class FuelPriceDetailViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.detailFuelPriceTextView)
        TextView detailFuelPriceTextView;
        @BindView(R.id.detailFuelTypeTextView)
        TextView detailFuelTypeTextView;

        public FuelPriceDetailViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
