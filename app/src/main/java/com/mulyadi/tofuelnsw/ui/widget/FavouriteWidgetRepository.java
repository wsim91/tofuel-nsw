package com.mulyadi.tofuelnsw.ui.widget;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.ui.base.BaseRepository;
import com.mulyadi.tofuelnsw.ui.main.DataFetchedEventListener;


public interface FavouriteWidgetRepository extends BaseRepository {

     void fetchWidgetDetails(Station widgetStations, DataFetchedEventListener dataFetchedEventListener);
}
