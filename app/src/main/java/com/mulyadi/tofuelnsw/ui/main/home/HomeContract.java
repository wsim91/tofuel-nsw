package com.mulyadi.tofuelnsw.ui.main.home;

import com.google.android.gms.maps.model.LatLngBounds;
import com.mulyadi.tofuelnsw.Data.UserLocation;
import com.mulyadi.tofuelnsw.Data.UserSetting;
import com.mulyadi.tofuelnsw.ui.base.BasePresenter;
import com.mulyadi.tofuelnsw.ui.base.BaseView;

import java.util.List;

/**
 * Created by Welly Mulyadi on 14/03/2018.
 */

public interface HomeContract {
    interface View extends BaseView{
        void showHistory(List<UserSetting> history);
        void showHistoryRetrievalError();
        void showNoHistoryFoundError();
        void hideError();
        void showLocationSearch(LatLngBounds searchBound);
        void showResultFindings(UserLocation userLocation);
    }

    interface Presenter extends BasePresenter{
        void searchLocation();
        void findMyLocation(boolean alreadyRequested);
        void deleteHistory(List<Integer> positionsToBeDeleted, HomeFragment.OnHistoryDeleteListener onHistoryDeleteListener);
    }
}
