package com.mulyadi.tofuelnsw.ui.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.Rest.FuelApiService;
import com.mulyadi.tofuelnsw.ui.main.FuelApiResponse.FuelByLocationResponse;
import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.Util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Welly Mulyadi on 18/08/2017.
 */

public class FuelRecViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    static final String LOG_TAG = FuelRecViewAdapter.class.getSimpleName();
    static final int FUEL_VIEW_TYPE = 0;
    static final int AD_VIEW_TYPE = 1;
    static final int FETCHED_TIME_VIEW_TYPE = 2;
    static final int AD_POSITION_FACTOR = 11; //every 10th position(index-based) is an ad view



    static final int LIST_ITEM_FUEL_LAYOUT_ID = R.layout.fuel_list_item;
    static final int LIST_ITEM_AD_LAYOUT_ID = R.layout.fuel_list_item_ad;
    static final int LIST_ITEM_FETCHED_TIME_LAYOUT_ID = R.layout.timestamp_list_item;

    private int extraViews = 1;

    private FuelByLocationResponse results;
    private  Context context;

    private List<AdView> adViews;
    private boolean offlineMode;


    RecyclerView mRecyclerView;
    OnStationClickedCallback stationClickedCallback;

    public FuelRecViewAdapter(Context context, FuelByLocationResponse results, OnStationClickedCallback stationClickedCallback){
        this.results = results;
        this.context = context;
        adViews = new ArrayList<>();
        this.stationClickedCallback = stationClickedCallback;
        this.offlineMode = false;
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rootView;
        RecyclerView.ViewHolder holder;

        switch(viewType) {
            case FUEL_VIEW_TYPE:
                rootView = LayoutInflater.from(parent.getContext()).inflate(LIST_ITEM_FUEL_LAYOUT_ID, parent, false);
                holder = new FuelViewHolder(rootView);
                break;
            case AD_VIEW_TYPE:
                rootView = LayoutInflater.from(parent.getContext()).inflate(LIST_ITEM_AD_LAYOUT_ID, parent, false);
                holder = new AdViewHolder(rootView);
                break;
            default:
                rootView = LayoutInflater.from(parent.getContext()).inflate(LIST_ITEM_FETCHED_TIME_LAYOUT_ID, parent, false);
                holder = new FetchedTimeViewHolder(rootView);
        }
        return holder;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
    }

    @Override
    public int getItemViewType(int position) {

        int viewType = FUEL_VIEW_TYPE;

        if(position == (getItemCount()-1)){
            viewType = FETCHED_TIME_VIEW_TYPE;
        }else if((position + 1) % AD_POSITION_FACTOR == 0){//skipping first item to be an adview and keep it 0-based indices
            viewType = AD_VIEW_TYPE;
        }
        return  viewType;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int currentViewType = getItemViewType(position);
        if(currentViewType == FUEL_VIEW_TYPE) {

            //Since the response result in 2 lists (station list and price list),
            //the price list is the one that is affected by sortby and sortascending query,
            //while the station list is there for reference (provide info of the station like address and code)
            //Due to this, the positioning of stations and price lists differs, and so we will need to fetch the station's
            //details by going through the stations list, see if they match up the station code give in price
            //UPDATE: As of 11-04-2018, the station list is now the effected list from the sorting query.
            //To accomodate this change, we'll need to match the price list's station code with the one in the station list.
            //To instead sort the price list would be inefficient and redundant since the station list is already sorted.

            final int dataPosition = getDataPosition(position);
            final int stationCode = results.getStations().get(dataPosition).getCode();

            int pricePosition = 0;

            for(int ix = 0; ix<results.getPrices().size();ix++){
                if(results.getPrices().get(ix).getStationCode() == stationCode){
                    pricePosition = ix;
                    break;
                }
            }

            final FuelViewHolder fuelViewHolder = (FuelViewHolder) holder;
            double price = results.getPrices().get(pricePosition).getPrice();
            String priceStr = "" + price;
            String fuelTypeStr = results.getPrices().get(pricePosition).getFuelType();
            fuelViewHolder.fuelPriceTextView.setText(priceStr);
            fuelViewHolder.fuelTypeTextView.setText(fuelTypeStr);

            if (position == results.getPrices().size() - 1) {
                fuelViewHolder.bottomDivider.setVisibility(View.INVISIBLE);
            } else {
                fuelViewHolder.bottomDivider.setVisibility(View.VISIBLE);
            }



            final Station station = results.getStations().get(dataPosition);

            int logoResource = Util.getStationDrawableResource(station.getBrand(), context);
            fuelViewHolder.stationBrandImageView.setImageResource(logoResource);
            fuelViewHolder.stationNameTextView.setText(station.getName());
            fuelViewHolder.stationAddressTextView.setText(station.getAddress());
            fuelViewHolder.distTextView.setText(context.getString(R.string.distance_string, station.getLocation().getDistance()));
            fuelViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stationClickedCallback.onStationClicked(station);
                }
            });


        }else if(currentViewType == AD_VIEW_TYPE){
            AdViewHolder adViewHolder = (AdViewHolder)holder;
            int adPosition = getExpectedAdViewItems(position);
            AdView adView = adViews.get(adPosition);
            //the view holder recycled might not be the same one for this exact position
            //so we'll need to clear it, otherwise we will get "java.lang.IllegalStateException:
            // The specified child already has a parent. You must call removeView() on the child's parent first."
            if(adViewHolder.adViewContainer.getChildCount() > 0){
                adViewHolder.adViewContainer.removeAllViews();
            }
            //since we're already got the adView, it might also be attached to a parent from a different position
            //we need to clear this too
            if(adView.getParent() != null){
                ViewGroup parent = (ViewGroup) adView.getParent();
                parent.removeView(adView);
            }
            adViewHolder.adViewContainer.addView(adView);
        }else{
            FetchedTimeViewHolder fetchedTimeViewHolder = (FetchedTimeViewHolder)holder;
            String friendlyDate = Util.getDateFriendly(results.getUserSetting().getTimeFetched());
            if(friendlyDate == null){
                friendlyDate = results.getUserSetting().getTimeFetched();
            }
            fetchedTimeViewHolder.fetchedTimeTextView.setText(friendlyDate);
            fetchedTimeViewHolder.expandedOfflineLayout.setVisibility(offlineMode?View.VISIBLE:View.GONE);
        }
    }

    public void clearData(){
        this.results = null;
        adViews.clear();
        notifyDataSetChanged();
    }

    public void updateData(FuelByLocationResponse fuelByLocationResponse){
        this.results = fuelByLocationResponse;
        int noOfAdViews = getNoOfAdViews(fuelByLocationResponse.getStations().size());
        String adId = context.getString(R.string.test_unit_id);
        for(int ix = 0; ix < noOfAdViews; ix++){
            AdView adView = new AdView(context);
            adView.setAdSize(AdSize.BANNER);
            adView.setAdUnitId(adId);
            adView.loadAd(new AdRequest.Builder().build());
            adViews.add(adView);
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        int itemCount = 0;

        if(results != null && results.getStations() != null && results.getStations().size() > 0){
            int noOfAdViews = getNoOfAdViews(results.getStations().size());
            itemCount = results.getStations().size() + noOfAdViews + extraViews;
        }

        return itemCount;
    }

    public void setOfflineMode(boolean offlineMode){
        this.offlineMode = offlineMode;
    }


    public class FuelViewHolder extends RecyclerView.ViewHolder{


        private final String LOG_TAG = FuelViewHolder.class.getSimpleName();

        @BindView(R.id.stationBrandImageView)
        ImageView stationBrandImageView;
        @BindView(R.id.stationNameTextView)
        TextView stationNameTextView;
        @BindView(R.id.fuelPriceTextView)
        TextView fuelPriceTextView;
        @BindView(R.id.fuelTypeTextView)
        TextView fuelTypeTextView;
        @BindView(R.id.distTextView)
        TextView distTextView;
        @BindView(R.id.bottomDivider)
        View bottomDivider;
        @BindView(R.id.stationAddressTextView)
        TextView stationAddressTextView;


        View rootView;


        //As mentioned in Google IO 2016, it is crucial to use the whole view for the expanding/collapsing animation
        //to do so, it requires a ViewGroup
        public FuelViewHolder(View rootView){
            super(rootView);

            ButterKnife.bind(this, rootView);
            this.rootView = rootView;

        }
    }


    public class AdViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.adViewContainer)
        FrameLayout adViewContainer;

        public AdViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }



    public class FetchedTimeViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.fetchedTimeTextView)
        TextView fetchedTimeTextView;
        @BindView(R.id.expandedOfflineLayout)
        View expandedOfflineLayout;

        public FetchedTimeViewHolder(View rootView){

            super(rootView);
            try {
                ButterKnife.bind(this, rootView);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private int getDataPosition(int listPosition){
        return listPosition - getExpectedAdViewItems(listPosition);
    }
    /**
     */
    private int getNoOfAdViews(int size){

        int expectedTotalNoOfViews = size + getExpectedAdViewItems(size) + extraViews;
        return getExpectedAdViewItems(expectedTotalNoOfViews);
    }

    private int getExpectedAdViewItems(int size){
        return (int)Math.floor((size)/AD_POSITION_FACTOR);
    }


}
