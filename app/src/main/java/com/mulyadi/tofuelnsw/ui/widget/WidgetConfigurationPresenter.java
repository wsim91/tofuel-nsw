package com.mulyadi.tofuelnsw.ui.widget;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.Util.UserPreference;
import com.mulyadi.tofuelnsw.ui.main.AuthenticateUserCallback;
import com.mulyadi.tofuelnsw.ui.main.favourite.FavouritesRepository;
import com.mulyadi.tofuelnsw.ui.main.favourite.FetchFavouritesCallback;
import com.mulyadi.tofuelnsw.ui.widget.WidgetContract.WidgetConfigurationContract.*;

import java.util.List;

public class WidgetConfigurationPresenter implements Presenter {

    FavouritesRepository mRepository;
    View mView;
    UserPreference mUserPreference;

    public WidgetConfigurationPresenter(FavouritesRepository repository, View view, UserPreference userPreference) {
        mRepository = repository;
        mView = view;
        mUserPreference = userPreference;
    }


    @Override
    public void start() {
        mView.showLoading();
        mView.hideEmptyFavouritesInformation();
      if(mRepository.isFavDataLoaded()){
          setFavourites(mRepository.getLoadedFavourites());
          return;
      }

      loadFavourites();
    }

    private void loadFavourites() {
        mRepository.authenticateUser(getClass(), new AuthenticateUserCallback() {
            @Override
            public void authenticationSuccess() {
                mRepository.fetchFavourites(new FetchFavouritesCallback() {
                    @Override
                    public void onFetchComplete(List<Station> favourites) {
                        setFavourites(favourites);
                    }
                });
            }

            @Override
            public void authenticationFailure() {
                mView.hideLoading();
            }
        });

    }


    private void setFavourites(List<Station> favourites){
        mView.hideLoading();
        if(favourites != null && !favourites.isEmpty()) {
            mView.attachFavourites(favourites);
        }else{
            mView.clearFavourites();
            mView.showEmptyFavouritesInformation();
        }
    }

    @Override
    public void saveConfiguration(int id, Station selectedStation) {
        mUserPreference.saveWidgetConfig( id,selectedStation.getAddress());
        mView.completeConfiguration();
    }

    @Override
    public void stop() {
        mRepository.removeAuth(getClass());
        mView.dismissConfiguration();
    }

    @Override
    public void refresh() {

    }
}
