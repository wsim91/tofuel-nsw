package com.mulyadi.tofuelnsw.ui.main;

import com.mulyadi.tofuelnsw.ui.base.BasePresenter;

/**
 * Created by Welly Mulyadi on 19/03/2018.
 */

public interface MainContract{
    interface View{
        void showSignInPrompt();
        void launchUI();
        void showSignInFailedError();
        void hideSignInFailedError();
        void hideNavBar();
        void showNavBar();
        void hideSignInAction();
        void showSignInAction();
        void hideAppBar();
        void showAppBar();
        void showLoading();
        void hideLoading();

    }

    interface Presenter extends BasePresenter{
        void issueError();
        //Object for convenience of testing
        void linkUserToApp(Object acct);
    }
}
