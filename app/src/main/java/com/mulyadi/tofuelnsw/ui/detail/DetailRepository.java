package com.mulyadi.tofuelnsw.ui.detail;

import android.net.Uri;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.ui.base.BaseRepository;
import com.mulyadi.tofuelnsw.ui.main.DataFetchedEventListener;

/**
 * Created by Welly Mulyadi on 5/03/2018.
 */

public interface DetailRepository extends BaseRepository{
    void fetchDetails(DataFetchedEventListener dataFetchedEventListener);
    AllPetrolPriceByStationResponse getDetails();
    String getShareInformation();
    Uri getStationMapURI();
    void deleteStation(StationDeleteCallback stationDeleteCallback);
    void switchFavouriteStatus(FavouriteStatusListener favouriteStatusListener);
    void fetchFavouriteStatus(FavouriteStatusListener favouriteStatusListener);
    Station fetchStation();
    boolean getIsStationMarked();
}
