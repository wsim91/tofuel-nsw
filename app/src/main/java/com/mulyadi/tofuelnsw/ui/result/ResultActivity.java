package com.mulyadi.tofuelnsw.ui.result;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.Util.DevicePermissionHandler;
import com.mulyadi.tofuelnsw.Util.MyLocationFinder;
import com.mulyadi.tofuelnsw.Util.Util;
import com.mulyadi.tofuelnsw.ui.detail.DialogStationFragment;
import com.mulyadi.tofuelnsw.ui.main.home.HomeFragment;

import javax.xml.transform.Result;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultActivity extends AppCompatActivity implements DialogStationFragment.OnDeleteStationListener{

    final String LOG_TAG = ResultActivity.class.getSimpleName();

    @BindString(R.string.result_fragment_tag)
    String mFragmentTag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        ButterKnife.bind(this);

        //Add only if we don't have the fragment created
        //This will help us re-use the fragment, upon activty re-creation (orientation change)
        if(getSupportFragmentManager().getFragments() == null || getSupportFragmentManager().getFragments().size() == 0) {
            getSupportFragmentManager().beginTransaction().add(R.id.toFuelFragmentContainer, new ResultsFragment(), mFragmentTag).commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //When user's location service is turned off
        if(requestCode == MyLocationFinder.REQUEST_CHECK_SETTINGS){
            int statusCode = ResultsFragment.STATUS_OK;
            if(resultCode == MyLocationFinder.ENABLE_DEVICE_REQUIREMENT_FOR_GOOGLE_LOCATION_DENIED){
              statusCode = ResultsFragment.LOCATION_SERVICES_UNAVAILABLE;
            }
            Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(mFragmentTag);
            if (currentFragment != null) {
                ResultsFragment resultsFragment = (ResultsFragment) currentFragment;
                resultsFragment.requestLocationUpdates(statusCode);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onDeleteFavouriteStation(int positionDeleted) {
        //does nothing. Implementation of this listener is required but this is intended to only favouritesfragment
    }
}
