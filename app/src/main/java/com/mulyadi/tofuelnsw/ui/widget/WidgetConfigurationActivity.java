package com.mulyadi.tofuelnsw.ui.widget;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.Util.Repository;
import com.mulyadi.tofuelnsw.Util.UserPreference;
import com.mulyadi.tofuelnsw.Util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;



public class WidgetConfigurationActivity extends AppCompatActivity implements WidgetContract.WidgetConfigurationContract.View{


    @BindView(R.id.stationChooserCancelBtn)
    Button stationChooserCancelBtn;
    @BindView(R.id.stationChooserRecyclerView)
    RecyclerView stationChooserRecyclerView;
    @BindView(R.id.stationChooserLoadingProgressBar)
    ProgressBar stationChooserLoadingProgressBar;
    @BindView(R.id.stationChooserRecyclerViewEmptyTextView)
    TextView stationChooserRecyclerViewEmptyTextView;

    @BindString(R.string.stationAddressForWidget)
    String stationAddressWidgetIntentKey;
    int mAppWidgetId;
    Intent results;

    FavouriteStationChooserAdapter mAdapter;
    WidgetConfigurationPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_widget_chooser);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID);
        }

        if(mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID){
            finish();
        }

        //Need to set it to cancelled first, incase the user backs out of the configuration.
        //This way, the widget host is notified that the configuration was cancelled.
        results = new Intent();
        results.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_CANCELED, results);

        mAdapter = new FavouriteStationChooserAdapter(new ArrayList<Station>());
        stationChooserRecyclerView.setAdapter(mAdapter);


        stationChooserRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mPresenter = new WidgetConfigurationPresenter(Repository.getFavouritesInstance(this), this, UserPreference.getInstance(this));
        stationChooserCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.stop();
            }
        });
        mPresenter.start();



    }

    @Override
    public void attachFavourites(List<Station> favourites) {
        mAdapter.setFavourites(favourites);
    }

    @Override
    public void clearFavourites() {
        mAdapter.clear();
    }

    @Override
    public void showEmptyFavouritesInformation() {
        stationChooserRecyclerViewEmptyTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyFavouritesInformation() {
        stationChooserRecyclerViewEmptyTextView.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        stationChooserLoadingProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        stationChooserLoadingProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void dismissConfiguration() {
        finish();
    }

    @Override
    public void completeConfiguration() {

        FavouriteStationWidgetProvider.updateWidget(this, AppWidgetManager.getInstance(this), mAppWidgetId);
        setResult(RESULT_OK, results);
        finish();
    }

    class FavouriteStationChooserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        List<Station> favourites;

        public FavouriteStationChooserAdapter(List<Station> favourites) {
            this.favourites = favourites;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new StationWidgetChooserViewHolder(LayoutInflater.from(WidgetConfigurationActivity.this).inflate(R.layout.fuel_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            final StationWidgetChooserViewHolder stationWidgetChooserViewHolder = (StationWidgetChooserViewHolder)holder;

            stationWidgetChooserViewHolder.distTextView.setVisibility(View.GONE);
            stationWidgetChooserViewHolder.petrolLayout.setVisibility(View.GONE);
            int logoResource = Util.getStationDrawableResource(favourites.get(position).getBrand(), WidgetConfigurationActivity.this);

            stationWidgetChooserViewHolder.stationBrandImageView.setImageResource(logoResource);
            stationWidgetChooserViewHolder.stationNameTextView.setText(favourites.get(position).getName());
            stationWidgetChooserViewHolder.stationAddressTextView.setText(favourites.get(position).getAddress());

            stationWidgetChooserViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.saveConfiguration(mAppWidgetId, favourites.get(stationWidgetChooserViewHolder.getAdapterPosition()));
                }
            });
        }

        public void clear(){
            favourites.clear();
            notifyDataSetChanged();
        }
        @Override
        public int getItemCount() {
            return favourites.size();
        }

        public void setFavourites(List<Station> favourites){
            this.favourites = favourites;
            notifyDataSetChanged();
        }

        public class StationWidgetChooserViewHolder extends RecyclerView.ViewHolder{

            @BindView(R.id.stationBrandImageView)
            ImageView stationBrandImageView;
            @BindView(R.id.stationNameTextView)
            TextView stationNameTextView;
            @BindView(R.id.stationAddressTextView)
            TextView stationAddressTextView;
            //Need to hide the below. We're reusing same layout as results activity's item view, some of the views are irrelevant for widget config.
            @BindView(R.id.distTextView)
            TextView distTextView;
            @BindView(R.id.petrolPrefLayout)
            View petrolLayout;

            public StationWidgetChooserViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

    }
}
