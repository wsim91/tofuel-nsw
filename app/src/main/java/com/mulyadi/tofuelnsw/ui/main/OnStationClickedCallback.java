package com.mulyadi.tofuelnsw.ui.main;

import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;

/**
 * Created by Welly Mulyadi on 23/02/2018.
 */

public interface OnStationClickedCallback {
    void onStationClicked(Station station);
}
