package com.mulyadi.tofuelnsw.Rest;

import android.support.annotation.NonNull;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Welly Mulyadi on 23/08/2017.
 */

public class FuelApiClient {

    /**FUEL API RESPONSE CODE AND ERROR CODE**/
    public static final int OK_CODE = 200;
    public static final int TRAFFIC_LIMIT_EXCEEDED_ERROR_CODE = 408;
    public static final int INTERNAL_SERVER_ERROR_CODE = 500;

    /**CUSTOM-MADE ERROR_CODE FOR FUEL API**/
    public static final int NETWORK_ERROR_CODE = -1;
    public static final int DATA_UNAVAILABLE_ERROR_CODE = -2;

    private static Retrofit retrofit = null;


    public static Retrofit getFuelApiClient(@NonNull String baseUrl){

        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }
}
