package com.mulyadi.tofuelnsw.Rest;

import com.mulyadi.tofuelnsw.FuelApiRequest.FuelByLocationRequest;
import com.mulyadi.tofuelnsw.Oauth.AccessToken;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.ui.main.FuelApiResponse.FuelByLocationResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Welly Mulyadi on 23/08/2017.
 */

public interface FuelApiService {

    @GET("oauth/client_credential/accesstoken?grant_type=client_credentials")
    Call<AccessToken> getAccessToken(@Header("authorization") String auth);
    @Headers({
            "transactionid:1",
            "Content-Type:application/json; charset=utf-8"
    })
    @GET("FuelPriceCheck/v1/fuel/prices/station/q")
    Call<AllPetrolPriceByStationResponse> getFuelPricesForStation(@Header("apikey") String apiKey,
                                                                  @Header("Authorization") String accessToken,
                                                                  @Header("requesttimestamp")String requestTimeStamp,
                                                                  @Query("stationcode") String stationCode);

    @Headers({
            "transactionid:1",
            "Content-Type:application/json; charset=utf-8"
    })
    @POST("FuelPriceCheck/v1/fuel/prices/nearby")
    Call<FuelByLocationResponse> getNearBy(@Header("apikey") String apiKey,
                                           @Header("Authorization") String accessToken,
                                           @Header("requesttimestamp")String requestTimeStamp,
                                           @Body FuelByLocationRequest fuelByLocationRequest);
}
