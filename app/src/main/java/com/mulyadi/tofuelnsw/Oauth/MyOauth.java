package com.mulyadi.tofuelnsw.Oauth;

import com.mulyadi.tofuelnsw.Rest.FuelApiService;
import com.mulyadi.tofuelnsw.ui.AccessTokenEventListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Welly Mulyadi on 23/08/2017.
 */

public class MyOauth {

    private static final String LOG_TAG = MyOauth.class.getSimpleName();
    private static AccessToken accessToken = null;

    public static String getAccessToken(){
        return (accessToken!= null)?accessToken.getAccessTokenStr():null;
    }

    public static void fetchAccessToken(FuelApiService fuelApiService,String authBit64, String preAuthAccessToken, AccessTokenEventListener accessTokenEventListener){
        Call<AccessToken> accessTokenCall = fuelApiService.getAccessToken(authBit64);
        accessTokenCall.enqueue(new FetchAccessTokenCallBack(accessTokenEventListener, preAuthAccessToken));
    }


    private static class FetchAccessTokenCallBack implements Callback<AccessToken>{

        AccessTokenEventListener accessTokenEventListener;
        String preAuth;

        public FetchAccessTokenCallBack(AccessTokenEventListener accessTokenEventListener, String preAuth){
            super();
            this.accessTokenEventListener = accessTokenEventListener;
            this.preAuth = preAuth;
        }
        @Override
        public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
            accessToken = response.body();
            if(accessToken != null) {
                accessToken.setPreAuth(preAuth);
                accessTokenEventListener.onSuccess(accessToken.getAccessTokenStr());
            }else{
                accessTokenEventListener.onFailed();
            }
        }

        @Override
        public void onFailure(Call<AccessToken> call, Throwable t) {
            accessTokenEventListener.onFailed();
        }
    }

}
