package com.mulyadi.tofuelnsw.Oauth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Welly Mulyadi on 23/08/2017.
 */

public class AccessToken {
    @SerializedName("access_token")
    String accessToken;
    String preAuth = null;

    public String getAccessTokenStr() {
        return preAuth+accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setPreAuth(String preAuth){
        this.preAuth = preAuth;
    }
}
