package com.mulyadi.tofuelnsw.Util;

/**
 * Created by Welly Mulyadi on 20/03/2018.
 */

public interface PermissionHandler {
    boolean hasPermission(String permission);
    void requestPermission(String[] permissions, int requestCode);
    boolean isNetworkAvailable();
    boolean hasGPSEnabled();
}
