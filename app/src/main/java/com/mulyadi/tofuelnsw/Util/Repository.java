package com.mulyadi.tofuelnsw.Util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mulyadi.tofuelnsw.Data.MostRecent;
import com.mulyadi.tofuelnsw.Data.Price;
import com.mulyadi.tofuelnsw.Data.Station;
import com.mulyadi.tofuelnsw.Data.UserLocation;
import com.mulyadi.tofuelnsw.Data.UserSetting;
import com.mulyadi.tofuelnsw.FuelApiRequest.FuelByLocationRequest;
import com.mulyadi.tofuelnsw.Oauth.MyOauth;
import com.mulyadi.tofuelnsw.Rest.FuelApiClient;
import com.mulyadi.tofuelnsw.Rest.FuelApiService;
import com.mulyadi.tofuelnsw.ui.AccessTokenEventListener;
import com.mulyadi.tofuelnsw.ui.detail.DetailRepository;
import com.mulyadi.tofuelnsw.ui.detail.FavouriteStatusListener;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.ui.detail.StationDeleteCallback;
import com.mulyadi.tofuelnsw.ui.main.AuthenticateUserCallback;
import com.mulyadi.tofuelnsw.ui.main.DataFetchedEventListener;
import com.mulyadi.tofuelnsw.ui.main.FuelApiResponse.FuelByLocationResponse;
import com.mulyadi.tofuelnsw.ui.main.MainRepository;
import com.mulyadi.tofuelnsw.ui.main.favourite.FavouritesRepository;
import com.mulyadi.tofuelnsw.ui.main.favourite.FetchFavouritesCallback;
import com.mulyadi.tofuelnsw.ui.main.home.FetchHistoryCallback;
import com.mulyadi.tofuelnsw.ui.main.home.HomeRepository;
import com.mulyadi.tofuelnsw.ui.result.MostRecentFetchCallback;
import com.mulyadi.tofuelnsw.ui.result.ResultRepository;
import com.mulyadi.tofuelnsw.ui.widget.FavouriteWidgetRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mulyadi.tofuelnsw.Rest.FuelApiClient.*;

/**
 * Created by Welly Mulyadi on 2/03/2018.
 */

public class Repository implements MainRepository, ResultRepository, DetailRepository, FavouritesRepository, HomeRepository, FavouriteWidgetRepository{

    private static final String LOG_TAG = Repository.class.getSimpleName();
    public static final double NSW_LAT_ONE = -33.880490;
    public static final double NSW_LONG_ONE = 151.184363;
    public static final double NSW_LAT_TWO = -33.858754;
    public static final double NSW_LONG_TWO = 151.229596;
    //used to determine whether to fetch local cache or new data from API. time limitation is presented in milliseconds
    public static final long REFRESH_TIME = 300000;

    protected FirebaseDatabase mFirebaseDatabase;
    protected FuelApiService mFuelApiService;
    protected DatabaseReference mMostRecentRef, mLocationHistoryRef, mFavRef;
    protected FirebaseAuth mFirebaseAuth;
    private ConnectivityManager mConMgr;
    private Preference mUserPref;
    private AllPetrolPriceByStationResponse mStationDetails;
    private Station mStation;
    private boolean isStationMarked;

    private boolean mIsFavouritesUpToDate;
    private List<Station> mFavourites;

    private Map<Class, FirebaseAuth.AuthStateListener> mAuthStateListeners;

    /**
     * FOR ResultRepository
     */
    private boolean mIsResultUpToDate = false;
    private FuelByLocationResponse mResults;
    private UserLocation mUserLocation;
    /**
     * FOR HomeRepository
     */
    private boolean mIsHistoryUpToDate = false;
    private List<UserSetting> mHistory;

    static Repository mRepository = null;

    private Repository(Context context) {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseDatabase.setPersistenceEnabled(true);
        mUserPref = UserPreference.getInstance(context);
        Util.setUserPref(mUserPref);
        mFuelApiService = FuelApiClient.getFuelApiClient(mUserPref.getFuelApiBaseUrl()).create(FuelApiService.class);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mConMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        mAuthStateListeners = new HashMap<>();
    }

    public static Repository getInstance(Context context){
        if(mRepository == null){
            mRepository = new Repository(context);
        }

        return mRepository;
    }

    public static MainRepository getMainInstance(Context context){
        return getInstance(context);
    }

    public static HomeRepository getHomeInstance(Context context){
        return getInstance(context);
    }

    public static FavouriteWidgetRepository getWidgetInstance(Context context){
        return getInstance(context);
    }

    public static ResultRepository getResultInstance(Context context, UserLocation userLocation){
        return getInstance(context).setUserLocation(userLocation);
    }

    public static DetailRepository getDetailInstance(Context context, Station station){
        return getInstance(context).setSelectedStation(station);
    }

    public static FavouritesRepository getFavouritesInstance(Context context){
        return getInstance(context);
    }

    @Override
    public boolean hadAskUserToSignIn() {
        return mUserPref.getSignInRequestStatus();
    }

    @Override
    public void setAskUserToSignIn() {
        mUserPref.setSignInRequestStatus();
    }

    private Repository setUserLocation(UserLocation userLocation){

        if(userLocation != null && isNewResults(userLocation)){
            mUserLocation = userLocation;
            mResults = null;
        }
        return mRepository;
    }

    private Repository setSelectedStation(Station station){
        if(isNewDetail(station)){
            mStation = station;
            mStationDetails = null;
            isStationMarked = false;
        }
        return mRepository;
    }

    private boolean isNewResults(UserLocation userLocation){
        if(mResults == null || mUserLocation == null)
            return true;

        if(mUserLocation.getAddress().equals(userLocation.getAddress()) && Util.getTimeDifferenceToNow(mResults.getUserSetting().getTimeFetched()) < 60000){
            return false;
        }

        return true;
    }
    private boolean isNewDetail(Station station){
        if(mStationDetails == null || mStation == null)
            return true;

        if(mStation.getAddress().equals(station.getAddress()) && Util.getTimeDifferenceToNow(mStationDetails.getTimeAccessed()) < 60000){
            return false;
        }

        return true;
    }


    @Override
    public boolean getIsStationMarked() {
        return isStationMarked;
    }

    @Override
    public void linkUserToApp(Object acct, final AuthenticateUserCallback authenticateUserCallback) {
        if(acct instanceof GoogleSignInAccount){
            GoogleSignInAccount gAcct = (GoogleSignInAccount) acct;
            AuthCredential credential = GoogleAuthProvider.getCredential(gAcct.getIdToken(), null);
            mFirebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        authenticateUserCallback.authenticationSuccess();
                    }else{
                        authenticateUserCallback.authenticationFailure();
                    }
                }
            });
        }else{
            authenticateUserCallback.authenticationFailure();
        }
    }

    @Override
    public boolean isAppOnline() {
        return Util.hasInternetConnection(mConMgr);
    }

    protected void setUpUserDBReferences(){
        FirebaseUser user = mFirebaseAuth.getCurrentUser();
        if(user != null ) {
            if(mLocationHistoryRef == null || mMostRecentRef == null || mFavRef == null) {
                mLocationHistoryRef = mFirebaseDatabase.getReference().child(user.getUid()).child("History");
                mMostRecentRef = mFirebaseDatabase.getReference().child(user.getUid()).child("Most Recent");
                mFavRef = mFirebaseDatabase.getReference().child(user.getUid()).child("Favorites");
            }

        }
    }

    @Override
    public void fetchResults(final DataFetchedEventListener dataFetchedEventListener) {
        String accessToken = MyOauth.getAccessToken();
        if (accessToken == null) {
            MyOauth.fetchAccessToken(mFuelApiService, mUserPref.getAuthBit64(), mUserPref.getPreAuthAccessTokenString(), new AccessTokenEventListener() {
                @Override
                public void onSuccess(String accessToken) {
                    fetchFuelByLocation(accessToken, dataFetchedEventListener);
                }

                @Override
                public void onFailed() {
                    dataFetchedEventListener.onFailDataFetched(NETWORK_ERROR_CODE);
                }
            });
        } else {
            //fetch petrol station
            fetchFuelByLocation(accessToken, dataFetchedEventListener);
        }

    }

    @Override
    public boolean isCacheResultUpToDate() {
         if(mIsResultUpToDate && mResults != null){
             if(Util.getTimeDifferenceToNow(mResults.getUserSetting().getTimeFetched()) < REFRESH_TIME){
                return true;
             }
         }

         return false;
    }

    @Override
    public void setResultUpToDate(boolean isUpToDate) {
        mIsResultUpToDate = isUpToDate;
    }

    @Override
    public String getAddress() {
        return (mUserLocation != null) ?mUserLocation.getAddress():null;
    }

    @Override
    public boolean validateAddress(String address) {
        return address.contains(mUserPref.getNswCode()) || address.contains(mUserPref.getNswFull());
    }

    @Override
    public UserLocation getUserLocation() {
        return mUserLocation;
    }

    @Override
    public FuelByLocationResponse getCachedResult(boolean updateLocalHistory) {
        if(updateLocalHistory){
            checkForDuplicateOrOverflow(mResults.getUserSetting());
        }
        return mResults;
    }

    @Override
    public void fetchMostRecent(final MostRecentFetchCallback mostRecentFetchCallback) {

        if(mUserPref.hasConnectedToFrdb(UserPreference.RECENT_LOCATION)) {

            mMostRecentRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean recentFound = false;
                    MostRecent mostRecent = dataSnapshot.getValue(MostRecent.class);
                    if (mostRecent != null && mostRecent.getRecentResults() != null) {
                        UserLocation mostRecentUserLoc = mostRecent.getRecentResults().getUserSetting().getUserLocation();
                        if (mostRecentUserLoc.getLatitude() == mUserLocation.getLatitude() &&
                                mostRecentUserLoc.getLongitude() == mUserLocation.getLongitude()) {
                            mResults = mostRecent.getRecentResults();
                            mostRecentFetchCallback.onMostRecentFetched(mostRecent.getRecentResults());
                            recentFound = true;
                        }
                    }
                    if (!recentFound)
                        mostRecentFetchCallback.onMostRecentFetched(null);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                    mostRecentFetchCallback.onMostRecentFetched(null);
                }
            });
        }else{
            mostRecentFetchCallback.onMostRecentFetched(null);
        }
    }

    private void fetchFuelByLocation(String accessToken, DataFetchedEventListener dataFetchedEventListener){
        UserSetting userSetting = getUserSetting();
        FuelByLocationRequest fuelByLocationRequest = new FuelByLocationRequest(userSetting.getFuelType(),
                userSetting.getBrand().toArray(new String[userSetting.getBrand().size()]),
                mUserLocation.getLatitude(),
                mUserLocation.getLongitude(),
                userSetting.getRadius(),
                userSetting.getSortBy() ,
                userSetting.isSortAscending());
        Call<FuelByLocationResponse> fuelByLocationResponseCall = mFuelApiService.getNearBy(mUserPref.getApiKey(),accessToken, Util.getTimeStamp(), fuelByLocationRequest);
        fuelByLocationResponseCall.enqueue(new FetchStationsCallback( userSetting, dataFetchedEventListener));
    }

    private UserSetting getUserSetting(){
        String[] brand = mUserPref.getPrefBrand();
        String fuelType = mUserPref.getPrefFuelType();
        int radius = mUserPref.getPrefRadius();
        String sortBy = mUserPref.getPrefSortBy();
        boolean sortAscending = mUserPref.isSortAscPreferred();

        return new UserSetting(convertStringArrToList(brand), fuelType, sortBy, radius, sortAscending, mUserLocation);
    }

    private List<String> convertStringArrToList(String[] arr){

        List<String> list = new ArrayList<>();
        Collections.addAll(list, arr);
        return list;
    }


    private void addToCache(MostRecent mostRecent){
        mResults = mostRecent.getRecentResults();
        mMostRecentRef.setValue(mostRecent);
        mUserPref.setConnectionStatusOfFrdb(UserPreference.RECENT_LOCATION);
    }

    private class FetchStationsCallback implements Callback<FuelByLocationResponse> {

        private final String LOG_TAG = FetchStationsCallback.class.getSimpleName();
        //private FuelRecViewAdapter fuelRecViewAdapter;
        private UserSetting currentUserSetting;
        private DataFetchedEventListener dataFetchedEventListener;

        private FetchStationsCallback(UserSetting userSetting, DataFetchedEventListener dataFetchedEventListener){
            this.currentUserSetting = userSetting;
            this.dataFetchedEventListener = dataFetchedEventListener;
        }

        @Override
        public void onResponse(Call<FuelByLocationResponse> call, Response<FuelByLocationResponse> response) {
            FuelByLocationResponse fuelByLocationResponse = response.body();

            if(fuelByLocationResponse != null) {
                fuelByLocationResponse = Util.trimStations(fuelByLocationResponse, currentUserSetting.getRadius());


                if (fuelByLocationResponse.getStations() != null
                        && fuelByLocationResponse.getPrices() != null
                        && fuelByLocationResponse.getStations().size() > 0
                        && fuelByLocationResponse.getPrices().size() > 0) {

                    currentUserSetting.setTimeFetched(Util.getTimeStamp());
                    fuelByLocationResponse.setUserSetting(currentUserSetting);

                    checkForDuplicateOrOverflow(currentUserSetting);
                    setResultUpToDate(true);
                    setHistoryStatus(false);

                    dataFetchedEventListener.onSuccessDataFetched(fuelByLocationResponse);
                    addToCache(new MostRecent(fuelByLocationResponse));
                } else {
                    dataFetchedEventListener.onFailDataFetched(DATA_UNAVAILABLE_ERROR_CODE);
                }
            }
        }

        @Override
        public void onFailure(Call<FuelByLocationResponse> call, Throwable t) {
            //handle "timeout" error message from t.getMessage()
            Util.logDebug(LOG_TAG,t.getMessage());
            dataFetchedEventListener.onFailDataFetched(NETWORK_ERROR_CODE);
        }


    }

    private void checkForDuplicateOrOverflow(final UserSetting currentUserSetting){
        mLocationHistoryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                boolean removedDuplicate = false;
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    UserSetting userSetting = child.getValue(UserSetting.class);

                    if (userSetting != null &&userSetting.getUserLocation()
                            .getAddress()
                            .equals(currentUserSetting.getUserLocation().getAddress())) {
                        child.getRef().removeValue();
                        removedDuplicate = true;
                    }
                }

                if (!removedDuplicate && dataSnapshot.getChildrenCount() == mUserPref.getMaxSearches()) {
                    dataSnapshot.getChildren().iterator().next().getRef().removeValue();
                }

                mLocationHistoryRef.push().setValue(currentUserSetting);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void fetchDetails(final DataFetchedEventListener dataFetchedEventListener){
        if(mFirebaseAuth.getCurrentUser() != null) {
            setUpUserDBReferences();
            String accessToken = MyOauth.getAccessToken();
            if (accessToken == null) {
                MyOauth.fetchAccessToken(mFuelApiService, mUserPref.getAuthBit64(), mUserPref.getPreAuthAccessTokenString(),new AccessTokenEventListener() {
                    @Override
                    public void onSuccess(String accessToken) {
                        requestStationDetails(mStation, accessToken, dataFetchedEventListener);
                    }

                    @Override
                    public void onFailed() {
                        dataFetchedEventListener.onFailDataFetched(NETWORK_ERROR_CODE);
                    }
                });
            } else {
                requestStationDetails(mStation, accessToken, dataFetchedEventListener);
            }
        }
    }

    @Override
    public void fetchWidgetDetails(final Station station, final DataFetchedEventListener dataFetchedEventListener) {
        if(mFirebaseAuth.getCurrentUser() != null) {
            setUpUserDBReferences();
            String accessToken = MyOauth.getAccessToken();
            if (accessToken == null) {
                MyOauth.fetchAccessToken(mFuelApiService, mUserPref.getAuthBit64(),mUserPref.getPreAuthAccessTokenString(),new AccessTokenEventListener() {
                    @Override
                    public void onSuccess(String accessToken) {
                        requestWidgetDetails(station, accessToken, dataFetchedEventListener);
                    }

                    @Override
                    public void onFailed() {
                        dataFetchedEventListener.onFailDataFetched(NETWORK_ERROR_CODE);
                    }
                });
            } else {
                requestWidgetDetails(station, accessToken, dataFetchedEventListener);
            }
        }
    }

    private void requestWidgetDetails (Station widgetStation, String accessToken, DataFetchedEventListener dataFetchedEventListener){

            requestStationDetails(widgetStation, accessToken, dataFetchedEventListener);

    }

    @Override
    public Station fetchStation() {
        return mStation;
    }

    @Override
    public AllPetrolPriceByStationResponse getDetails() {
        return mStationDetails;
    }

    @Override
    public String getShareInformation() {
        StringBuilder shareStrBlder = new StringBuilder();
        shareStrBlder.append(mUserPref.bindShareStationName(mStation.getName()));
        shareStrBlder.append(mUserPref.bindShareStationAddress(mStation.getAddress()));
        for(int ix = 0; ix < mStationDetails.getPrices().size(); ix++){
            Price currentPrice = mStationDetails.getPrices().get(ix);
            shareStrBlder.append(mUserPref.bindShareStationFuelInfo(currentPrice.getFuelType(),currentPrice.getPrice()));
        }
        shareStrBlder.append(mUserPref.bindShareStationTimeAccessed(mStationDetails.getTimeAccessed()));
        shareStrBlder.append(mUserPref.getShareAppInfo());

        return shareStrBlder.toString();
    }

    @Override
    public Uri getStationMapURI() {
        //Future TODO: place id/lat and long to get more accurate information in map
        return  Uri.parse(mUserPref.bindShareStationMapUri(mStation.getAddress()));
    }



    private void requestStationDetails(Station station, String accessToken, DataFetchedEventListener dataFetchedEventListener) {
        Call<AllPetrolPriceByStationResponse> requestCall = mFuelApiService.getFuelPricesForStation(mUserPref.getApiKey(),accessToken, Util.getTimeStamp(), String.valueOf(station.getCode()));
        String fuelType = station.getPrefFuelType();
        if (fuelType == null || fuelType.isEmpty()) {
            fuelType = mUserPref.getPrefFuelType();
        }
        requestCall.enqueue(new FetchAllPricesForStationCallback(dataFetchedEventListener, fuelType));
    }

    @Override
    public void switchFavouriteStatus(final FavouriteStatusListener favouriteStatusListener) {
        if(isStationMarked){
            mFavRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for(DataSnapshot entry : dataSnapshot.getChildren()) {
                        Station currentStation = entry.getValue(Station.class);
                        if(currentStation != null &&
                                currentStation.getLocation().getLongitude() == mStation.getLocation().getLongitude() &&
                                currentStation.getLocation().getLatitude() == mStation.getLocation().getLatitude()) {
                            entry.getRef().removeValue();
                            isStationMarked = false;
                            setFavouritesUpToDate(false);
                            favouriteStatusListener.onStatusChecked(isStationMarked);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else{
            mStation.setPrefFuelType(mUserPref.getPrefFuelType());
            mFavRef.push().setValue(mStation, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError == null){
                        isStationMarked = true;
                        setFavouritesUpToDate(false);
                        favouriteStatusListener.onStatusChecked(isStationMarked);
                    }
                }
            });
        }


    }

    @Override
    public void fetchFavouriteStatus(final FavouriteStatusListener favouriteStatusListener) {
        mFavRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean isFound = false;
                for(DataSnapshot entry : dataSnapshot.getChildren()) {
                    Station currentStation = entry.getValue(Station.class);
                    if(currentStation != null &&
                            currentStation.getLocation().getLongitude() == mStation.getLocation().getLongitude() &&
                            currentStation.getLocation().getLatitude() == mStation.getLocation().getLatitude()){
                        isFound = true;
                        break;
                    }
                }
                isStationMarked = isFound;
                favouriteStatusListener.onStatusChecked(isFound);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void deleteStation(final StationDeleteCallback stationDeleteCallback) {
        if(isAppOnline()) {
            mFavRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    int position = 0;
                    for (DataSnapshot entry : dataSnapshot.getChildren()) {
                        Station currentStation = entry.getValue(Station.class);
                        if (currentStation != null &&
                                currentStation.getLocation().getLongitude() == mStation.getLocation().getLongitude() &&
                                currentStation.getLocation().getLatitude() == mStation.getLocation().getLatitude()) {
                            final int positionToBeDeleted = position;
                            entry.getRef().removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    stationDeleteCallback.deleteComplete(mStation.getName(), positionToBeDeleted);
                                    setFavouritesUpToDate(false);
                                }
                            });
                            break;
                        }
                        position += 1;
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else{
            stationDeleteCallback.deleteFailed();
        }
    }

    @Override
    public void setFavouritesUpToDate(boolean upToDate) {
        mIsFavouritesUpToDate = upToDate;
    }

    @Override
    public boolean isFavDataLoaded() {
        return (mFavourites != null && mIsFavouritesUpToDate);
    }

    @Override
    public void fetchFavourites(final FetchFavouritesCallback fetchFavouritesCallback) {

        if(!isFavDataLoaded()) {
            setUpUserDBReferences();
            mFavRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (mFavourites == null) {
                        mFavourites = new ArrayList<>();
                    }
                    mFavourites.clear();

                    for (DataSnapshot entry : dataSnapshot.getChildren()) {
                        mFavourites.add(entry.getValue(Station.class));
                    }

                    setFavouritesUpToDate(true);
                    fetchFavouritesCallback.onFetchComplete(mFavourites);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else{
            fetchFavouritesCallback.onFetchComplete(mFavourites);
        }
    }

    @Override
    public List<Station> getLoadedFavourites() {
        return mFavourites;
    }

    private class FetchAllPricesForStationCallback implements Callback<AllPetrolPriceByStationResponse> {

        private final String LOG_TAG = com.mulyadi.tofuelnsw.ui.detail.FetchAllPricesForStationCallback.class.getSimpleName();
        private DataFetchedEventListener mDataFetchedListener;
        private String mPrefFuel;

        public FetchAllPricesForStationCallback(DataFetchedEventListener dataFetchedEventListener, String prefFuel){
            mDataFetchedListener = dataFetchedEventListener;
            mPrefFuel = prefFuel;
        }
        @Override
        public void onResponse(Call<AllPetrolPriceByStationResponse> call, Response<AllPetrolPriceByStationResponse> response) {
            AllPetrolPriceByStationResponse result = response.body();
            //moves the preferred to the top
            if(result != null && result.getPrices() != null && result.getPrices().size() > 0) {
                for (int ix = 0; ix < result.getPrices().size(); ix++) {
                    if (result.getPrices().get(ix).getFuelType().equals(mPrefFuel)) {
                        result.getPrices().add(0, result.getPrices().remove(ix));
                        break;
                    }
                }
                mStationDetails = result;
                mStationDetails.setTimeAccessed(Util.getTimeStamp());
                mDataFetchedListener.onSuccessDataFetched(result);
            }else{
                mDataFetchedListener.onFailDataFetched(response.code() == OK_CODE ? DATA_UNAVAILABLE_ERROR_CODE : response.code());
            }

        }

        @Override
        public void onFailure(Call<AllPetrolPriceByStationResponse> call, Throwable t) {
            Util.logDebug(LOG_TAG, t.getMessage());
            //handle error message timeout
            mDataFetchedListener.onFailDataFetched(NETWORK_ERROR_CODE);
        }
    }

    @Override
    public void setHistoryStatus(boolean isUpToDate) {
        mIsHistoryUpToDate = isUpToDate;
    }

    @Override
    public void fetchHistory(final FetchHistoryCallback fetchHistoryCallback) {
        if(!isLocalHistoryDataUpToDate()) {
            /**
             * In the case of checking the DB for the first time, a db state check via mConnectionRef is required.
             * This because a connection with addListenerForSingleValueEvent() or addValueEventListener() directly to the db won't work in the case of first-use & no internet connection
             * mConnectionRef helps in determining if there's a connection exist already to the firebase database back-end (persistent or non-persistent).
             */
            if(mUserPref.hasConnectedToFrdb(UserPreference.HISTORY_LOCATION)||isAppOnline()) {
                mLocationHistoryRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mUserPref.setConnectionStatusOfFrdb(UserPreference.HISTORY_LOCATION);
                        if (mHistory == null) {
                            mHistory = new ArrayList<>();
                        }
                        mHistory.clear();

                        for (DataSnapshot entry : dataSnapshot.getChildren()) {
                            UserSetting historyEntry = entry.getValue(UserSetting.class);
                            if (historyEntry != null) {
                                mHistory.add(historyEntry);
                            }
                        }

                        setHistoryStatus(true);
                        fetchHistoryCallback.onFetched(mHistory);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        fetchHistoryCallback.onFailed();
                    }

                });
            }else{
                fetchHistoryCallback.onFailed();
            }
        }
    }

    @Override
    public void updateHistory() {
        mLocationHistoryRef.setValue(getLocalHistory());
    }

    @Override
    public boolean isLocalHistoryDataUpToDate() {
        return (mIsHistoryUpToDate && mHistory != null && mHistory.size() > 0);
    }

    @Override
    public List<UserSetting> getLocalHistory() {
        return mHistory;
    }


    @Override
    public LatLngBounds getSearchBound() {
        return new LatLngBounds(
                new LatLng(NSW_LAT_ONE, NSW_LONG_ONE),
                new LatLng(NSW_LAT_TWO, NSW_LONG_TWO));
    }

    @Override
    public boolean isUserAuthenticated() {
        return mFirebaseAuth.getCurrentUser() != null;
    }

    @Override
    public synchronized void authenticateUser(Class source, final AuthenticateUserCallback authenticateUserCallback){

        if(!mAuthStateListeners.containsKey(source) || mAuthStateListeners.get(source) == null){
            mAuthStateListeners.put(source, new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if(user != null){
                        setUpUserDBReferences();
                        authenticateUserCallback.authenticationSuccess();
                    }else{
                        authenticateUserCallback.authenticationFailure();
                    }
                }
            });
        }
        mFirebaseAuth.addAuthStateListener(mAuthStateListeners.get(source));
    }
    @Override
    public void removeAuth(Class source){
        if(mAuthStateListeners.containsKey(source) && mAuthStateListeners.get(source) != null) {
            FirebaseAuth.AuthStateListener currentAuthStateListener = mAuthStateListeners.remove(source);
            mFirebaseAuth.removeAuthStateListener(currentAuthStateListener);
            currentAuthStateListener = null;
        }
    }


}
