package com.mulyadi.tofuelnsw.Util;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static android.content.Context.LOCATION_SERVICE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.location.LocationManager.GPS_PROVIDER;


/**
 * Created by Welly Mulyadi on 20/03/2018.
 */

public class DevicePermissionHandler implements PermissionHandler{


    private AppCompatActivity mActivity;
    private Fragment mFragment;

    public DevicePermissionHandler(AppCompatActivity activity) {
        mActivity = activity;
        mFragment = null;
    }

    public DevicePermissionHandler(Fragment fragment){
        mFragment = fragment;
        mActivity = null;
    }

    @Override
    public boolean hasPermission(String permission) {
        int permissionCheck = (mActivity != null )
                ? ContextCompat.checkSelfPermission(mActivity, permission)
                : ContextCompat.checkSelfPermission(mFragment.getContext(), permission);

        return (permissionCheck == PERMISSION_GRANTED);
    }

    @Override
    public void requestPermission(String[] permissions, int requestCode) {
        if(mActivity != null)
            ActivityCompat.requestPermissions(mActivity, permissions, requestCode);
        else
            mFragment.requestPermissions(permissions, requestCode);
    }

    @Override
    public boolean isNetworkAvailable() {


        ConnectivityManager connectivityManager = (mActivity != null)
                ?(ConnectivityManager)mActivity.getSystemService(CONNECTIVITY_SERVICE)
                :(ConnectivityManager)mFragment.getContext().getSystemService(CONNECTIVITY_SERVICE);

        return (connectivityManager.getActiveNetworkInfo()!=null &&
                connectivityManager.getActiveNetworkInfo().isConnected());
    }

    @Override
    public boolean hasGPSEnabled() {

        LocationManager locationManager = (mActivity != null)
                ?(LocationManager)mActivity.getSystemService(LOCATION_SERVICE)
                :(LocationManager)mFragment.getContext().getSystemService(LOCATION_SERVICE);

        return locationManager != null && locationManager.isProviderEnabled(GPS_PROVIDER);
    }
}
