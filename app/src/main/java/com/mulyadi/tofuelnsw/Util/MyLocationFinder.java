package com.mulyadi.tofuelnsw.Util;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.mulyadi.tofuelnsw.Data.UserLocation;
import com.mulyadi.tofuelnsw.FetchAddressIntentService;
import com.mulyadi.tofuelnsw.ui.result.ResultsFragment;

import static com.mulyadi.tofuelnsw.FetchAddressIntentService.SUCCESS_RESULT;

/**
 * Created by Welly Mulyadi on 20/03/2018.
 */

public class MyLocationFinder implements LocationFinder{
    private static final String LOG_TAG = MyLocationFinder.class.getSimpleName();
    public static final int REQUEST_CHECK_SETTINGS = 100;
    public static final int ENABLE_DEVICE_REQUIREMENT_FOR_GOOGLE_LOCATION_DENIED = 0;
    public static final int ENABLE_DEVICE_REQUIREMENT_FOR_GOOGLE_LOCATION_ALLOWED = -1;

    private FusedLocationProviderClient mFusedLocationProvideClient;
    private SettingsClient mSettingsClient;
    private Activity mActivity;
    private Preference mPref;
    private LocationCallback mLocationCallback;
    private FetchLocationCallback mFetchLocationCallback;
    private LocationRequest mLocationRequest;

    public MyLocationFinder(Activity activity) {
        this.mFusedLocationProvideClient = LocationServices.getFusedLocationProviderClient(activity);
        this.mSettingsClient = LocationServices.getSettingsClient(activity);
        this.mActivity = activity;
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mPref = UserPreference.getInstance(mActivity);
        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if(locationResult == null){
                    return;
                }
                compileUserLocation(locationResult.getLastLocation());
                stopLocationUpdates();
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {

                if(!locationAvailability.isLocationAvailable()){
                    stopLocationUpdates();
                    mFetchLocationCallback.onFailed(ResultsFragment.LOCATION_SERVICES_UNAVAILABLE);
                }else {
                    super.onLocationAvailability(locationAvailability);
                }
            }
        };

        mFetchLocationCallback = null;
    }

    @Override
    public void fetchMyLocation(final FetchLocationCallback fetchLocationCallback) {
        if(fetchLocationCallback == null){
            return;
        }
        try{
            setFetchLocationCallback(fetchLocationCallback);
            mFusedLocationProvideClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if(location == null){
                        startLocationUpdates();
                        return;
                    }
                    compileUserLocation(location);
                }
            });
        }catch(SecurityException e){
            //will get here for failed security reasons (permissions).
            //will not get here if the fused location couldn't get last location, that is handled if the resulted location is null above.
            fetchLocationCallback.onFailed(ResultsFragment.NO_SECURITY_PERMISSION_CODE);
        }
    }

    private void setFetchLocationCallback(FetchLocationCallback fetchLocationCallback){
        mFetchLocationCallback = fetchLocationCallback;
    }

    private void startLocationUpdates(){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);

        mSettingsClient.checkLocationSettings(builder.build())
                .addOnSuccessListener(mActivity, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        try {
                            mFusedLocationProvideClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
                        }catch (SecurityException se){
                        }
                    }
                })
                .addOnFailureListener(mActivity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException)e).getStatusCode();
                        switch(statusCode){
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    ((ResolvableApiException) e).startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                                }catch (IntentSender.SendIntentException sie){
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                break;
                        }
                    }
                });

    }

    private void stopLocationUpdates(){
        mFusedLocationProvideClient.removeLocationUpdates(mLocationCallback);
    }

    /**
     * Helper method that helps to extract the address from the given location object and compile it together to a newly created UserLocation object.
     * The UserLocation object will then be passed on to the FetchLocationCallback object that was provided in fetchMyLocation() method
     * @param location
     */
    private void  compileUserLocation(final Location location){
        Intent intent = new Intent(mActivity, FetchAddressIntentService.class);
        intent.putExtra(mPref.getAddressReceiverKey(), new ResultReceiver(new Handler()){
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode == SUCCESS_RESULT) {
                    UserLocation userLocation = new UserLocation(location);
                    String userAddress = resultData.getString(mPref.getAddressResultDataKey());
                    userLocation.setAddress(userAddress);
                    mFetchLocationCallback.onFetched(userLocation);
                }
            }

        });
        intent.putExtra(mPref.getLocationDataKey(), location);
        mActivity.startService(intent);
    }
}
