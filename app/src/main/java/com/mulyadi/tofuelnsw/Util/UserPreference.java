package com.mulyadi.tofuelnsw.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.preference.PreferenceManager;

import com.mulyadi.tofuelnsw.R;

/**
 * Created by Welly Mulyadi on 9/03/2018.
 */

public class UserPreference implements Preference{

    private static UserPreference INSTANCE;
    private static final String LOG_TAG = UserPreference.class.getSimpleName();
    private Resources mResources;
    private SharedPreferences mSharedPreferences;
    public static final int HISTORY_LOCATION = 1;
    public static final int RECENT_LOCATION = 2;

    private UserPreference(Resources resources, SharedPreferences sharedPreferences){
        mResources = resources;
        mSharedPreferences = sharedPreferences;
    }

    public static UserPreference getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = new UserPreference(context.getResources(), PreferenceManager.getDefaultSharedPreferences(context));
        }
        return INSTANCE;
    }

    @Override
    public String getApiKey() {
        return mResources.getString(R.string.fuel_api_key);
    }

    @Override
    public String getAuthBit64() {
        return mResources.getString(R.string.fuel_api_auth_bit_64);
    }

    @Override
    public String getPreAuthAccessTokenString() {
        return mResources.getString(R.string.pre_auth_access_token);
    }

    @Override
    public boolean getSignInRequestStatus() {
        return mSharedPreferences.getBoolean(mResources.getString(R.string.sign_in), false);
    }

    @Override
    public void setSignInRequestStatus() {
        mSharedPreferences.edit().putBoolean(mResources.getString(R.string.sign_in), true).apply();
    }

    @Override
    public String getFuelApiBaseUrl() {
        return mResources.getString(R.string.fuel_api_base_url);
    }

    @Override
    public String getPlacesApiKey() {
        return mResources.getString(R.string.google_places_api_key);
    }

    @Override
    public String getNswCode() {
        return mResources.getString(R.string.nsw_abbreviation);
    }

    @Override
    public String getNswFull() {
        return mResources.getString(R.string.nsw_full);
    }

    @Override
    public String getFuelApiDateFormat() {
        return mResources.getString(R.string.fuel_api_date_format);
    }

    @Override
    public String getAddressSplit() {
        return mResources.getString(R.string.address_split);
    }

    @Override
    public String getNswRegex() {
        return mResources.getString(R.string.nsw_regex);
    }

    @Override
    public String getAddressReceiverKey() {
        return mResources.getString(R.string.address_receiver);
    }

    @Override
    public String getAddressResultDataKey() {
        return mResources.getString(R.string.address_result_str_key);
    }

    @Override
    public String getLocationDataKey() {
        return mResources.getString(R.string.location_data_key);
    }

    @Override
    public String getGoogleSignInIdTokenKey() {
        return mResources.getString(R.string.google_signin_request_id_token);
    }

    @Override
    public String[] getPrefBrand() {
        return mResources.getStringArray(R.array.supported_brands);
    }

    @Override
    public String getWidgetIdKey() {
        return mResources.getString(R.string.widget_station_id_key);
    }

    @Override
    public String getWidgetEmptyDataString(String stationName) {
        return mResources.getString(R.string.widget_station_detail_str_empty_data, stationName);
    }

    @Override
    public String getWidgetStationCodeKey() {
        return mResources.getString(R.string.widget_station_code_key);
    }

    @Override
    public String getPrefFuelType() {
        int prefFuelIndex = mSharedPreferences.getInt(mResources.getString(R.string.fueltype_key), 0);
        return mResources.getStringArray(R.array.pref_fuel_list_entries_values)[prefFuelIndex];
    }

    @Override
    public int getPrefRadius() {
        int radiusIndex = mSharedPreferences.getInt(mResources.getString(R.string.radius_key), 0);
        return Integer.parseInt(mResources.getStringArray(R.array.pref_radius_entries_and_entries_values)[radiusIndex]);
    }

    @Override
    public String getPrefSortBy() {
        return mSharedPreferences.getString(mResources.getString(R.string.sort_by_key), mResources.getString(R.string.distance_entry_value));
    }

    @Override
    public boolean hasConnectedToFrdb(int frdbLocation) {
        String initialKey;

        switch(frdbLocation){
            case HISTORY_LOCATION:
                initialKey = mResources.getString(R.string.history_connection_status);
                break;
            default:
                initialKey = mResources.getString(R.string.recent_connection_status);
        }

        return mSharedPreferences.getBoolean(initialKey, false);
    }

    @Override
    public void setConnectionStatusOfFrdb(int frdbLocation) {
        String initialKey;

        switch(frdbLocation){
            case HISTORY_LOCATION:
                initialKey = mResources.getString(R.string.history_connection_status);
                break;
            default:
                initialKey = mResources.getString(R.string.recent_connection_status);
        }

        if(!mSharedPreferences.getBoolean(initialKey,false)){
            mSharedPreferences.edit().putBoolean(initialKey, true).apply();
        }
    }

    @Override
    public boolean isSortAscPreferred() {
        return mSharedPreferences.getBoolean(mResources.getString(R.string.sort_ascending_key), mResources.getBoolean(R.bool.ascending_default_entry));
    }

    @Override
    public int getMaxSearches() {
        return mResources.getInteger(R.integer.max_searches);
    }


    @Override
    public String bindShareStationName(String name) {
        return mResources.getString(R.string.share_str_station_name, name);
    }

    @Override
    public String bindShareStationAddress(String address) {
        return mResources.getString(R.string.share_str_station_address, address);
    }

    @Override
    public String bindShareStationFuelInfo(String fuelType, double price) {
        return mResources.getString(R.string.share_str_price,
                fuelType,
                price);
    }

    @Override
    public String bindShareStationTimeAccessed(String timeAccessed) {
        return mResources.getString(R.string.share_str_timestamp, timeAccessed);
    }

    @Override
    public String getShareAppInfo() {
        return mResources.getString(R.string.share_str_app_info);
    }

    @Override
    public String bindShareStationMapUri(String address) {
        return mResources.getString(R.string.map_placename_pin_str,  address.replace(" ","+"));
    }

   private String getWidgetConfigKey(int id) {
        return mResources.getString(R.string.stationAddressForWidget, id);
    }

    private String getWidgetDetailUpdateTimeKey(int id){
        return mResources.getString(R.string.stationDetailUpdatedTimeWidget, id);
    }

    @Override
    public void saveWidgetConfig(int id, String configValue) {
        mSharedPreferences.edit().putString(getWidgetConfigKey(id), configValue).apply();
    }

    @Override
    public String loadWidgetConfig(int id) {
        return mSharedPreferences.getString(getWidgetConfigKey(id), null);
    }

    @Override
    public void deleteWidgetConfig(int id) {
        mSharedPreferences.edit().remove(getWidgetConfigKey(id)).apply();
        mSharedPreferences.edit().remove(getWidgetDetailUpdateTimeKey(id)).apply();
    }

    @Override
    public void saveWidgetLastUpdatedTime(int id, String time) {
        mSharedPreferences.edit().putString(getWidgetDetailUpdateTimeKey(id), time).apply();
    }

    @Override
    public String loadWidgetLastUpdatedTime(int id) {
        return mSharedPreferences.getString(getWidgetDetailUpdateTimeKey(id), null);
    }
}
