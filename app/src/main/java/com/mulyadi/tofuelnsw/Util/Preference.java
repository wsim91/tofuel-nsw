package com.mulyadi.tofuelnsw.Util;


/**
 * Created by mulya on 9/03/2018.
 */

public interface Preference {
    String getApiKey();
    String getAuthBit64();
    String getPreAuthAccessTokenString();
    String getFuelApiBaseUrl();
    String getPlacesApiKey();
    String getNswCode();
    String getNswFull();
    String getFuelApiDateFormat();
    String getAddressSplit();
    String getNswRegex();
    String getAddressReceiverKey();
    String getAddressResultDataKey();
    String getLocationDataKey();
    String getGoogleSignInIdTokenKey();
    String[] getPrefBrand();
    String getPrefFuelType();
    String getPrefSortBy();
    boolean getSignInRequestStatus();
    void setSignInRequestStatus();
    boolean hasConnectedToFrdb(int frdbLocation);
    void setConnectionStatusOfFrdb(int frdbLocation);
    String getWidgetIdKey();
    String getWidgetEmptyDataString(String stationName);
    String getWidgetStationCodeKey();
    int getPrefRadius();
    boolean isSortAscPreferred();
    int getMaxSearches();
    String bindShareStationName(String name);
    String bindShareStationAddress(String address);
    String bindShareStationFuelInfo(String fuelType, double price);
    String bindShareStationTimeAccessed(String timeAccessed);
    String bindShareStationMapUri(String address);
    String getShareAppInfo();
    void saveWidgetConfig(int id, String configValue);
    String loadWidgetConfig(int id);
    void deleteWidgetConfig(int id);

    void saveWidgetLastUpdatedTime(int id, String time);
    String loadWidgetLastUpdatedTime(int id);


}
