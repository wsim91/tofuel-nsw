package com.mulyadi.tofuelnsw.Util;

/**
 * Created by Welly Mulyadi on 22/02/2018.
 */

public interface SnackbarActionCallback {
    void onSnackbarActionClicked();
}
