package com.mulyadi.tofuelnsw.Util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.PreferenceManager;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.firebase.ui.auth.data.model.User;
import com.mulyadi.tofuelnsw.Data.Price;
import com.mulyadi.tofuelnsw.Data.UserLocation;
import com.mulyadi.tofuelnsw.Data.UserSetting;
import com.mulyadi.tofuelnsw.R;
import com.mulyadi.tofuelnsw.ui.detail.FuelApiResponse.AllPetrolPriceByStationResponse;
import com.mulyadi.tofuelnsw.ui.main.FuelApiResponse.FuelByLocationResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Welly Mulyadi on 16/08/2017.
 */

public class Util {

    /**set to false to hide debug log messages in logcat, true otherwise**/
    public static boolean DEBUG = true;
    private static final String LOG_TAG = Util.class.getSimpleName();

    private static Preference userPref = null;
    public static final long MOST_RECENT_CACHE_EXPIRATION_IN_MILLIS = 30000;

    private static FuelByLocationResponse mockFuelByLocationResponse;
    private static AllPetrolPriceByStationResponse mockAppPetrolPriceByStationResponse;
    private static  SimpleDateFormat fuelAPIDateFormat;


    public static HashMap<String, String> petrolGlossary = null;
    public static HashMap<String, Integer> stationLogoGlossary = null;

    public static void logDebug(String tag, String logMsg){
        if(DEBUG) Log.d(tag, logMsg);
    }


    public static boolean hasInternetConnection(ConnectivityManager connMgr){
        boolean hasConnection = false;
        NetworkInfo netInfo = connMgr.getActiveNetworkInfo();

        if(netInfo != null && netInfo.isConnected()){
            hasConnection = true;
        }
        return hasConnection;
    }

    public static void setUserPref(Preference userPreference){
        userPref = userPreference;
    }

    public static void launchIntent(Context context, Class activityClass, String extraKey, Parcelable obj){
        Intent intent = new Intent(context, activityClass);
        intent.putExtra(extraKey, obj);
        context.startActivity(intent);
    }
    public static SimpleDateFormat getDateFormat(){

        if(fuelAPIDateFormat == null){
            fuelAPIDateFormat = new SimpleDateFormat(userPref.getFuelApiDateFormat());
        }

        return fuelAPIDateFormat;
    }
    public static CharSequence getCorrectPrefixDate(Context context, Calendar givenDate){
        Calendar today = Calendar.getInstance();
        today.setTimeInMillis(System.currentTimeMillis());

        CharSequence datePrefix = null;

        //case when NYE(31 dec) and NYD(01 jan)
        if(today.get(Calendar.YEAR) - 1 == givenDate.get(Calendar.YEAR)){
            //>=365 due to leap year cases
           if(today.get(Calendar.DAY_OF_YEAR) == 1 && givenDate.get(Calendar.DAY_OF_YEAR) >=365){
               datePrefix = context.getString(R.string.history_date_prefix_yesterday);
           }
         //normal yesterday case
        }else if (today.get(Calendar.YEAR) == givenDate.get(Calendar.YEAR)){
            if(today.get(Calendar.DAY_OF_YEAR) - 1 == givenDate.get(Calendar.DAY_OF_YEAR)){
                datePrefix = context.getString(R.string.history_date_prefix_yesterday);
            }
        }

        return (datePrefix == null)?DateUtils.getRelativeTimeSpanString(context, givenDate.getTimeInMillis(), false):datePrefix;
    }

    public static void showDialog(Context context, String dialogMsg, int dialogConfirmStrId, int dialogCancelStrId, final DialogCallback dialogCallback){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(dialogMsg);
        builder.setPositiveButton(dialogConfirmStrId, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialogCallback.confirmedClicked();
            }
        });
        builder.setNegativeButton(dialogCancelStrId, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static String getTimeStamp(){
        Calendar calendar = Calendar.getInstance();

        return getDateFormat().format(calendar.getTime());
    }

    public static long getTimeDifferenceToNow(String timeStr){
        long time = getTimeInMillis(timeStr);

        return System.currentTimeMillis() - time;
    }

    public static String getDateFriendly(String source){
        try {
            Date sourceDate = getDateFormat().parse(source);


            DateFormat friendlyDf = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);

            return friendlyDf.format(sourceDate);
        }catch (ParseException e){
            logDebug(LOG_TAG, e.toString());
        }

        return null;
    }

    public static long getTimeInMillis(String dateStr){
        long timeInMillis;
        try {
            Date date = getDateFormat().parse(dateStr);
            timeInMillis = date.getTime();
            return timeInMillis;
        }catch (ParseException e){
            Util.logDebug(LOG_TAG, e.toString());
        }
        return -1;
    }

    /**The API is still in BETA stage.
     * /It has a flaw that if no results are within the queried radius, it will still fetch stations outside of the range,
     * which  is not what we want. So, we will need to do some in-house data management here by removing any fetched stations outside of the radius
     * @param fuelByLocationResponse
     * @param queriedRadius
     * @return
     */
    public static FuelByLocationResponse trimStations(FuelByLocationResponse fuelByLocationResponse, int queriedRadius){

        int noOfStations = fuelByLocationResponse.getStations().size();

        for(int currentStation = 0; currentStation < noOfStations; currentStation++){
            if(fuelByLocationResponse.getStations().get(currentStation).getLocation().getDistance() > queriedRadius){
                fuelByLocationResponse.getStations().remove(currentStation);
                currentStation -= 1;
                noOfStations -= 1;
            }
        }
        return fuelByLocationResponse;
    }

    public static String getSuburb(String address){
        String area[];

        String parts[] = address.split(userPref.getAddressSplit());
        int suburbIndex;

        //This means address (street no/street name is supplied)
        if(parts.length == 3){
            suburbIndex = 1;
        }else{
            //else street no/street name not supplied
            suburbIndex = 0;
        }

        area = parts[suburbIndex].split(userPref.getNswRegex());

        return area[0].trim();
    }

    public static String getPetrolString(String key, Context context){

        if(petrolGlossary == null){
            petrolGlossary = new HashMap<>();
            petrolGlossary.put(context.getString(R.string.premium_95_entry_value),
                    context.getString(R.string.premium_95_entry));
            petrolGlossary.put(context.getString(R.string.premium_98_entry_value),
                    context.getString(R.string.premium_98_entry));
            petrolGlossary.put(context.getString(R.string.unleaded_91_entry_value),
                    context.getString(R.string.unleaded_91_entry));
            petrolGlossary.put(context.getString(R.string.ethanol_94_entry_value),
                    context.getString(R.string.ethanol_94_entry));
            petrolGlossary.put(context.getString(R.string.diesel_entry_value),
                    context.getString(R.string.diesel_entry));
            petrolGlossary.put(context.getString(R.string.ethanol_105_entry_value),
                    context.getString(R.string.ethanol_105_entry));
            petrolGlossary.put(context.getString(R.string.premium_diesel_entry_value),
                    context.getString(R.string.premium_diesel_entry));
            petrolGlossary.put(context.getString(R.string.biodiesel_20_entry_value),
                    context.getString(R.string.biodiesel_20_entry));
            petrolGlossary.put(context.getString(R.string.LPG_entry_value),
                    context.getString(R.string.LPG_entry));
            petrolGlossary.put(context.getString(R.string.cng_ngv_entry_value),
                    context.getString(R.string.cng_ngv_entry));
        }


        return (petrolGlossary.containsKey(key)) ?petrolGlossary.get(key):null;
    }

    public static int getStationDrawableResource(String key, Context context){

        if(stationLogoGlossary == null){
            stationLogoGlossary = new HashMap<>();
            stationLogoGlossary.put(context.getString(R.string.seven_eleven_brand),
                    R.drawable.ic_7_eleven);
            stationLogoGlossary.put(context.getString(R.string.bp_brand),
                    R.drawable.ic_bp);
            stationLogoGlossary.put(context.getString(R.string.shell_brand),
                    R.drawable.ic_shell);
            stationLogoGlossary.put(context.getString(R.string.caltex_brand),
                    R.drawable.ic_caltex);
            stationLogoGlossary.put(context.getString(R.string.caltex_woolworths_brand),
                    R.drawable.ic_caltex_wools);
            stationLogoGlossary.put(context.getString(R.string.united_brand),
                    R.drawable.ic_united);
            stationLogoGlossary.put(context.getString(R.string.metrofuel_brand),
                    R.drawable.ic_metro);
            stationLogoGlossary.put(context.getString(R.string.budget_brand),
                    R.drawable.ic_budget_petrol);
            stationLogoGlossary.put(context.getString(R.string.coles_express_brand),
                    R.drawable.ic_coles_express);
        }


        return (stationLogoGlossary.containsKey(key)) ? stationLogoGlossary.get(key): R.drawable.ic_independent;
    }

    public static String trimAddress(String address){

        StringBuilder trimmedAddress = new StringBuilder();

        String parts[] = address.split(getSuburb(address));

        trimmedAddress.append(parts[0]);
        trimmedAddress.append(parts[1]);

        return trimmedAddress.toString().trim();
    }
}
