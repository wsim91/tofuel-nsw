package com.mulyadi.tofuelnsw.Util;

import com.mulyadi.tofuelnsw.Data.UserLocation;

/**
 * Created by Welly Mulyadi on 20/03/2018.
 */

public interface FetchLocationCallback {
    void onFetched(UserLocation userLocation);
    void onFailed(int errCode);
}
