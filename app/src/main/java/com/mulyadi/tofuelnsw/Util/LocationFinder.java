package com.mulyadi.tofuelnsw.Util;

/**
 * Created by Welly Mulyadi on 20/03/2018.
 */

public interface LocationFinder {
    void fetchMyLocation(FetchLocationCallback fetchLocationCallback);
}
