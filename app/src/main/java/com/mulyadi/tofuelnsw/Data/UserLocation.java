package com.mulyadi.tofuelnsw.Data;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.location.places.Place;

/**
 * Created by Welly Mulyadi on 14/09/2017.
 */

public class UserLocation implements Parcelable {

    private double latitude, longitude;
    private String address;


    public UserLocation(){
        //empty constructor required for frdb
    }
    public UserLocation(Place place){
       this.latitude = place.getLatLng().latitude;
       this.longitude = place.getLatLng().longitude;
       this.address = place.getAddress().toString();
    }

    public UserLocation(Location location){
        this.latitude =location.getLatitude();
        this.longitude = location.getLongitude();
        this.address = null;

    }

    private UserLocation(Parcel in){
        latitude = in.readDouble();
        longitude = in.readDouble();
        address = in.readString();
    }

    public String getAddress() {
        return (address == null)? "" : address ;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude(){
        return latitude;
    }

    public double getLongitude(){
        return longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public static final Parcelable.Creator<UserLocation> CREATOR = new Parcelable.Creator<UserLocation>(){
        @Override
        public UserLocation createFromParcel(Parcel parcel) {
            return new UserLocation(parcel);
        }

        @Override
        public UserLocation[] newArray(int i) {
            return new UserLocation[i];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeString(address);
    }
}
