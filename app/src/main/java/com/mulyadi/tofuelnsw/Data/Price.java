package com.mulyadi.tofuelnsw.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Welly Mulyadi on 20/09/2017.
 */

public class Price implements Parcelable {
    @SerializedName("fueltype")
    String fuelType;
    @SerializedName("price")
    double price;
    @SerializedName("lastupdated")
    String lastUpdated;

    //required for FRDB purposes
    public Price(){

    }
    //Mock Data purposes only
    public Price(String fuelType, double price, String lastUpdated){
        this.fuelType = fuelType;
        this.price = price;
        this.lastUpdated = lastUpdated;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    protected Price(Parcel in) {
        fuelType = in.readString();
        price = in.readDouble();
        lastUpdated = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fuelType);
        dest.writeDouble(price);
        dest.writeString(lastUpdated);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Price> CREATOR = new Parcelable.Creator<Price>() {
        @Override
        public Price createFromParcel(Parcel in) {
            return new Price(in);
        }

        @Override
        public Price[] newArray(int size) {
            return new Price[size];
        }
    };
}