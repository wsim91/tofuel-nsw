package com.mulyadi.tofuelnsw.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Welly Mulyadi on 23/08/2017.
 */

public class StationLocation implements Parcelable{

    @SerializedName("latitude")
    double latitude;
    @SerializedName("longitude")
    double longitude;
    @SerializedName("distance")
    double distance;

    //required for FRDB purposes
    public StationLocation(){

    }
    //Mock Data purposes only
    public StationLocation(double latitude, double longitude, double distance){
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
    }
    private StationLocation(Parcel in){
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.distance = in.readDouble();
    }
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeDouble(distance);
    }
    public static final Parcelable.Creator<StationLocation> CREATOR = new Parcelable.Creator<StationLocation>(){
        @Override
        public StationLocation createFromParcel(Parcel parcel) {
            return new StationLocation(parcel);
        }

        @Override
        public StationLocation[] newArray(int i) {
            return new StationLocation[i];
        }
    };

}
