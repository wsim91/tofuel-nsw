package com.mulyadi.tofuelnsw.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Welly Mulyadi on 18/08/2017.
 */

public class Station implements Parcelable {

    @SerializedName("brand")
    String brand;
    @SerializedName("code")
    int code;
    @SerializedName("name")
    String name;
    @SerializedName("address")
    String address;
    @SerializedName("location")
    StationLocation location;
    String prefFuelType;


    public Station(){
        //required for FRDB purposes
    }
    //Mock Data purposes only
    public Station(String brand, int code, String name, String address, StationLocation location, String prefFuelType){
        this.brand = brand;
        this.code = code;
        this.name = name;
        this.address = address;
        this.location = location;
        this.prefFuelType = prefFuelType;
    }



    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public StationLocation getLocation() {
        return location;
    }

    public void setLocation(StationLocation location) {
        this.location = location;
    }

    public String getPrefFuelType() {
        return prefFuelType;
    }

    public void setPrefFuelType(String prefFuelType) {
        this.prefFuelType = prefFuelType;
    }

    protected Station(Parcel in) {
        brand = in.readString();
        code = in.readInt();
        name = in.readString();
        address = in.readString();
        location = (StationLocation) in.readValue(StationLocation.class.getClassLoader());
        prefFuelType = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(brand);
        dest.writeInt(code);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeValue(location);
        dest.writeString(prefFuelType);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Station> CREATOR = new Parcelable.Creator<Station>() {
        @Override
        public Station createFromParcel(Parcel in) {
            return new Station(in);
        }

        @Override
        public Station[] newArray(int size) {
            return new Station[size];
        }
    };
}