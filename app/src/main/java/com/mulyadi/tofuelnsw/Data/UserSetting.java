package com.mulyadi.tofuelnsw.Data;

import java.util.List;

/**
 * Created by Welly Mulyadi on 8/11/2017.
 */

public class UserSetting {

    List<String> brand;
    String fuelType, sortBy;
    int radius;
    boolean sortAscending;
    private UserLocation userLocation;
    private String timeFetched;


    public UserSetting(){
        //default empty constructor for FRDB
    }

    public UserSetting(List<String> brand, String fuelType, String sortBy, int radius, boolean sortAscending, UserLocation userLocation){

        this.brand = brand;
        this.fuelType = fuelType;
        this.sortAscending = sortAscending;
        this.sortBy = sortBy;
        this.radius = radius;
        this.userLocation = userLocation;
    }

    public UserLocation getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(UserLocation userLocation) {
        this.userLocation = userLocation;
    }

    public List<String> getBrand() {
        return brand;
    }

    public void setBrand(List<String> brand) {
       this.brand = brand;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public boolean isSortAscending() {
        return sortAscending;
    }

    public void setSortAscending(boolean sortAscending) {
        this.sortAscending = sortAscending;
    }

    public String getTimeFetched() {
        return timeFetched;
    }

    public void setTimeFetched(String timeFetched) {
        this.timeFetched = timeFetched;
    }
}
