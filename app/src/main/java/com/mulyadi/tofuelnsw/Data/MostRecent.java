package com.mulyadi.tofuelnsw.Data;

import com.mulyadi.tofuelnsw.ui.main.FuelApiResponse.FuelByLocationResponse;

/**
 * Created by Welly Mulyadi on 19/02/2018.
 */

public class MostRecent {

    FuelByLocationResponse recentResults;

    public MostRecent() {
    }

    public MostRecent(FuelByLocationResponse recentResults) {
        this.recentResults = recentResults;
    }

    public FuelByLocationResponse getRecentResults() {
        return recentResults;
    }

    public void setRecentResults(FuelByLocationResponse recentResults) {
        this.recentResults = recentResults;
    }


}
