package com.mulyadi.tofuelnsw.Data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Welly Mulyadi on 23/08/2017.
 */

public class StationPrice extends Price {
    @SerializedName("stationcode")
    int stationCode;
    @SerializedName("priceunit")
    String priceUnit;
    @SerializedName("description")
    String description;


    //required for FRDB Purposes
    public StationPrice(){
        super();
    }
    public StationPrice(String fuelType, double price, String lastUpdated, int stationCode, String priceUnit, String description){
        super(fuelType, price, lastUpdated);

        this.stationCode = stationCode;
        this.priceUnit = priceUnit;
        this.description = description;


    }

    public int getStationCode() {
        return stationCode;
    }

    public void setStationCode(int stationCode) {
        this.stationCode = stationCode;
    }

    public String getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(String priceUnit) {
        this.priceUnit = priceUnit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
